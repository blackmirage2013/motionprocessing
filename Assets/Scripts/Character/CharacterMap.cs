﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMap : MonoBehaviour
{
    public Animator animator;
    public Dictionary<string, Transform> map18; // 18point
    public void Awake()
    {
        map18 = new Dictionary<string, Transform>(){
        {"Hips",  animator.GetBoneTransform(HumanBodyBones.Hips) },
                {"hip",  animator.GetBoneTransform(HumanBodyBones.Hips) },
        {"Chest",  animator.GetBoneTransform(HumanBodyBones.Chest) },
              {"Spine2",  animator.GetBoneTransform(HumanBodyBones.Chest) },
                {"chest",  animator.GetBoneTransform(HumanBodyBones.Chest) },
        {"Neck",  animator.GetBoneTransform(HumanBodyBones.Neck) },
                {"neck",  animator.GetBoneTransform(HumanBodyBones.Neck) },
        {"Head",   animator.GetBoneTransform(HumanBodyBones.Head)},
                {"head",   animator.GetBoneTransform(HumanBodyBones.Head)},
        {"LeftCollar",  animator.GetBoneTransform(HumanBodyBones.LeftShoulder)},
                {"lCollar",  animator.GetBoneTransform(HumanBodyBones.LeftShoulder)},
                {"lShldr",  animator.GetBoneTransform(HumanBodyBones.LeftUpperArm)},
                {"LeftShoulder",  animator.GetBoneTransform(HumanBodyBones.LeftUpperArm)},
        {"LeftArm",  animator.GetBoneTransform(HumanBodyBones.LeftUpperArm)},
            {"LeftUpArm",  animator.GetBoneTransform(HumanBodyBones.LeftUpperArm)},
                {"lForeArm",  animator.GetBoneTransform(HumanBodyBones.LeftLowerArm)},
                {"LeftForeArm",  animator.GetBoneTransform(HumanBodyBones.LeftLowerArm)},
                {"LeftElbow",  animator.GetBoneTransform(HumanBodyBones.LeftLowerArm)},
        {"LeftLowArm",  animator.GetBoneTransform(HumanBodyBones.LeftLowerArm)},
                {"lHand",  animator.GetBoneTransform(HumanBodyBones.LeftHand)},
            {"LeftWrist",  animator.GetBoneTransform(HumanBodyBones.LeftHand)},
        {"LeftHand",  animator.GetBoneTransform(HumanBodyBones.LeftHand)},

        {"RightCollar",  animator.GetBoneTransform(HumanBodyBones.RightShoulder)},
        {"rCollar",  animator.GetBoneTransform(HumanBodyBones.RightShoulder)},
        {"RightShoulder",  animator.GetBoneTransform(HumanBodyBones.RightUpperArm)},
        {"rShldr",  animator.GetBoneTransform(HumanBodyBones.RightUpperArm)},
                 {"RightArm",  animator.GetBoneTransform(HumanBodyBones.RightUpperArm)},
        {"RightUpArm",  animator.GetBoneTransform(HumanBodyBones.RightUpperArm)},
            {"RightForeArm",  animator.GetBoneTransform(HumanBodyBones.RightLowerArm)},
            {"rForeArm",  animator.GetBoneTransform(HumanBodyBones.RightLowerArm)},
                {"RightElbow",  animator.GetBoneTransform(HumanBodyBones.RightLowerArm)},
        {"RightLowArm",  animator.GetBoneTransform(HumanBodyBones.RightLowerArm)},

                {"rHand",  animator.GetBoneTransform(HumanBodyBones.RightHand)},
                {"RightWrist",  animator.GetBoneTransform(HumanBodyBones.RightHand)},
        {"RightHand",  animator.GetBoneTransform(HumanBodyBones.RightHand)},

        {"LeftHip",  animator.GetBoneTransform(HumanBodyBones.LeftUpperLeg)},
                {"lThigh",  animator.GetBoneTransform(HumanBodyBones.LeftUpperLeg)},
        {"LeftUpLeg",  animator.GetBoneTransform(HumanBodyBones.LeftUpperLeg)},
        {"LeftKnee",  animator.GetBoneTransform(HumanBodyBones.LeftLowerLeg)},
                {"lShin",  animator.GetBoneTransform(HumanBodyBones.LeftLowerLeg)},
                        {"LeftLeg",  animator.GetBoneTransform(HumanBodyBones.LeftLowerLeg)},
        {"LeftLowLeg",  animator.GetBoneTransform(HumanBodyBones.LeftLowerLeg)},
                {"LFoot",  animator.GetBoneTransform(HumanBodyBones.LeftFoot)},
                {"lFoot",  animator.GetBoneTransform(HumanBodyBones.LeftFoot)},
        {"LeftFoot",  animator.GetBoneTransform(HumanBodyBones.LeftFoot)},
                        {"LeftAnkle",  animator.GetBoneTransform(HumanBodyBones.RightFoot)},
            {"rThigh",  animator.GetBoneTransform(HumanBodyBones.RightUpperLeg)},
        {"RightHip",  animator.GetBoneTransform(HumanBodyBones.RightUpperLeg)},
        {"RightUpLeg",  animator.GetBoneTransform(HumanBodyBones.RightUpperLeg)},
        {"rShin",  animator.GetBoneTransform(HumanBodyBones.RightLowerLeg)},
        {"RightKnee",  animator.GetBoneTransform(HumanBodyBones.RightLowerLeg)},
                {"RightLeg",  animator.GetBoneTransform(HumanBodyBones.RightLowerLeg)},
        {"RightLowLeg",  animator.GetBoneTransform(HumanBodyBones.RightLowerLeg)},
                {"rFoot",  animator.GetBoneTransform(HumanBodyBones.RightFoot)},
                {"RFoot",  animator.GetBoneTransform(HumanBodyBones.RightFoot)},
        {"RightFoot",  animator.GetBoneTransform(HumanBodyBones.RightFoot)},
                {"RightAnkle",  animator.GetBoneTransform(HumanBodyBones.RightFoot)},
        };
    }
}
