﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;

// TODO 暫時取消拉條控制

public class TimerPlayer : MonoBehaviour
{
    public static TimerPlayer instance;
    public bool isPlaying = false; // 是否在播放
    [Header("UI")]
    // public TrackControl tracking;
    public Button Ctl_Button;
    public Image Ctl_Image;
    public Sprite playSprite;
    public Sprite pauseSprite;

    [Header("Event")]
    // public FrameUpdate_Int frameUpdate_Int;
    // public FrameUpdate_Float frameUpdate_Float;
    public UnityEvent restart = new UnityEvent();

    private void OnEnable()
    {
        instance = this;
    }

    public void OnClickCtl_Button()
    {
        if (isPlaying)
        {
            Pause();
        }
        else
        {
            Play();
        }
    }
    public void ResetBtn()
    {
        for (int i = 0; i < BVH_MotionViewer.allViewers.Count; i++)
        {
            BVH_MotionViewer.allViewers[i].frame = 0;
        }
    }
    public void Play()
    {
        Ctl_Image.sprite = pauseSprite;
        isPlaying = true;
        for (int i=0;i<BVH_MotionViewer.allViewers.Count;i++)
        {
            BVH_MotionViewer.allViewers[i].Play();
        }
        //Transition Ver
        for (int i = 0; i < BVH_Transition_MotionViewer.allViewers.Count; i++)
        {
            BVH_Transition_MotionViewer.allViewers[i].Play();
        }
    }
    public void Pause()
    {
        Ctl_Image.sprite = playSprite;
        isPlaying = false;
        for (int i = 0; i < BVH_MotionViewer.allViewers.Count; i++)
        {
            BVH_MotionViewer.allViewers[i].Pause();
        }
        //Transition Ver
        for (int i = 0; i < BVH_Transition_MotionViewer.allViewers.Count; i++)
        {
            BVH_Transition_MotionViewer.allViewers[i].Pause();
        }
    }
}