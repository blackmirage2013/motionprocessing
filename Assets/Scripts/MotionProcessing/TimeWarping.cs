﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BVH_Motion;
using System;
public class TimeWarping
{
    struct TableData
    {
        public float cost;
        public int preI;
        public int preJ;
        public int nextI;
        public int nextJ;
        public int I;
        public int J;
        public int pathLen;
        public float x0;
        public float z0;
        public float theta;
        public bool set;
    }
    bool initComplete = false;
    float[] windowKernelWeight;
    public int sampleFrameNum = 3;
    int extraPointNum = 1;
    int slopeLimit = 3;
    BVH_Motion.Motion refMotion;
    BVH_Motion.Motion conMotion;
    List<Vector3[]> refMotionPointCloud;
    List<Vector3[]> contrastMotionPointCloud;
    TableData[,] table;
    TableData[,] costTable;
    float minCost = float.MaxValue;
    public float[] minPos = new float[2];
    public float thetaY = 0;
    public BVH_Motion.Motion alignedRefMotion;
    public BVH_Motion.Motion alignedConMotion;
    public List<float> costList = new List<float>();
    public List<int> refIndex = new List<int>();
    public List<int> conIndex = new List<int>();
    public void Init(int _sampleFrameNum, float[] _kernel)
    {
        if (_sampleFrameNum % 2 == 0)
        {
            Debug.Log("Timewarp init ERROR! sampleFrameNum must be odd number");
            return;
        }
        else if (_kernel.Length != _sampleFrameNum)
        {
            Debug.Log("Timewarp init ERROR! kernel length must be identical to sampleFrameNum");
            return;
        }
        sampleFrameNum = _sampleFrameNum;
        windowKernelWeight = _kernel;
        initComplete = true;
    }
    public void Init(BVH_Motion.Motion _refMotion, BVH_Motion.Motion _contrastMotion)
    {
        refMotion = _refMotion;
        conMotion = _contrastMotion;
        refMotionPointCloud = ConstructMotionPointCloud(_refMotion);
        contrastMotionPointCloud = ConstructMotionPointCloud(_contrastMotion);
        sampleFrameNum = 3;
        extraPointNum = 1;
        slopeLimit = 3;
        windowKernelWeight = new float[] { 0.25f, 0.5f, 0.25f };
        minCost = float.MaxValue;
        initComplete = true;
    }
    public void CreateCostTable()
    {
        //create table
        table = new TableData[refMotionPointCloud.Count, contrastMotionPointCloud.Count];
        costTable = new TableData[refMotionPointCloud.Count, contrastMotionPointCloud.Count];
        double pointWeight = 1.0 / refMotionPointCloud[0].Length;
        for (int i = 0; i < refMotionPointCloud.Count; i++)
        {
            for (int j = 0; j < contrastMotionPointCloud.Count; j++)
            {
                //find optimal transform
                double theta, x0, z0;
                double t0 = 0, t1 = 0, t2 = 0, t3 = 0, t4 = 0, t5 = 0;
                for (int k = 0; k < sampleFrameNum; k++)
                {
                    int offset = k - k % 2;
                    Vector3[] p1 = GetFrameData(refMotionPointCloud, i + offset);
                    Vector3[] p2 = GetFrameData(contrastMotionPointCloud, j + offset);
                    double weight = pointWeight * windowKernelWeight[k];
                    for (int pIdx = 0; pIdx < p1.Length; pIdx++)
                    {
                        t0 += weight * (p1[pIdx].x * p2[pIdx].z - p2[pIdx].x * p1[pIdx].z);
                        t1 += weight * p1[pIdx].x;
                        t2 += weight * p2[pIdx].z;
                        t3 += weight * p2[pIdx].x;
                        t4 += weight * p1[pIdx].z;
                        t5 += weight * (p1[pIdx].x * p2[pIdx].x + p2[pIdx].z * p1[pIdx].z);
                    }
                }

                theta = Math.Atan((t0 - t1 * t2 + t3 * t4) / (t5 - t1 * t3 - t2 * t4));
                x0 = t1 - Math.Cos(theta) * t3 - Math.Sin(theta) * t2;
                z0 = t4 - Math.Cos(theta) * t2 + Math.Sin(theta) * t3;
                theta = theta * Mathf.Rad2Deg;
                //calculate cost
                float dis = 0;
                for (int k = 0; k < sampleFrameNum; k++)
                {
                    int offset = k - k % 2;
                    Vector3[] p1 = GetFrameData(refMotionPointCloud, i + offset);
                    Vector3[] p2 = GetFrameData(contrastMotionPointCloud, j + offset);
                    for (int pIdx = 0; pIdx < p1.Length; pIdx++)
                    {
                        Vector3 p2Origin = p2[0];
                        Vector3 p2NewPos = new Vector3(p2Origin.x + (float)x0, p2Origin.y, p2Origin.z + (float)z0);
                        Vector3 newP2 = Quaternion.AngleAxis((float)theta, Vector3.up) * (p2[pIdx] - p2Origin) + p2NewPos;
                        dis += Vector3.Distance(p1[pIdx], newP2);
                    }
                }
                costTable[i, j].cost = dis;
                costTable[i, j].set = true;
                costTable[i, j].theta = (float)theta;
                costTable[i, j].x0 = (float)x0;
                costTable[i, j].z0 = (float)z0;
                if (dis < minCost)
                {
                    minCost = dis;
                    minPos[0] = i;
                    minPos[1] = j;
                    thetaY = (float)theta;
                }
            }
        }
    }
    public void Do(int aligningI, int aligningJ)
    {
        aligningI -= 1;
        aligningJ -= 1;
        CreateCostTable();
        //connect path
        {
            //init
            for (int i = 0; i < refMotionPointCloud.Count; i++)
            {
                table[i, 0] = costTable[i, 0];
                table[i, 0].preI = -1;
                table[i, 0].preJ = -1;
                table[i, 0].I = i;
                table[i, 0].J = 0;
            }
            for (int j = 1; j < contrastMotionPointCloud.Count; j++)
            {
                table[0, j] = costTable[0, j];
                table[0, j].preI = -1;
                table[0, j].preJ = -1;
                table[0, j].I = 0;
                table[0, j].J = j;
            }
            BackwardPath(aligningI, aligningJ);
            //forwardPath(alignedI, alignJ);
        }
        alignedConMotion = new BVH_Motion.Motion(conMotion);
        alignedRefMotion = new BVH_Motion.Motion(refMotion);
        costList = new List<float>();
        refIndex = new List<int>();
        conIndex = new List<int>();
        ConstructChildInfo(alignedConMotion.rootJointInfo, conMotion.rootJointInfo);
        ConstructChildInfo(alignedRefMotion.rootJointInfo, refMotion.rootJointInfo);
        TableData goal = table[aligningI, aligningJ];
        while (true)
        {
            if (goal.I != 0 && goal.J != 0)
            {
                int offsetI = goal.I - goal.preI, offsetJ = goal.J - goal.preJ;
                for (int offset = 0; offset < offsetJ; offset++)
                {
                    conIndex.Add(goal.J - offset);
                    RootJoint root = new RootJoint(conMotion.frameData[goal.J - offset]);
                    root.position.x += costTable[goal.I, goal.J].x0;
                    root.position.z += costTable[goal.I, goal.J].z0;
                    root.rotation = Quaternion.AngleAxis((float)costTable[goal.I, goal.J].theta, Vector3.up) * root.rotation;
                    alignedConMotion.frameData.Add(root); ;
                }
                for (int offset = 0; offset < offsetI; offset++)
                {
                    refIndex.Add(goal.I - offset);
                    alignedRefMotion.frameData.Add(new RootJoint(refMotion.frameData[goal.I - offset]));
                }
                costList.Add(costTable[goal.I, goal.J].cost);
                while (alignedRefMotion.frameData.Count != alignedConMotion.frameData.Count)
                {
                    if (alignedRefMotion.frameData.Count > alignedConMotion.frameData.Count)
                    {
                        conIndex.Add(goal.J);
                        RootJoint conJoint = new RootJoint(conMotion.frameData[goal.J]);
                        conJoint.position.x += costTable[goal.I, goal.J].x0;
                        conJoint.position.z += costTable[goal.I, goal.J].z0;
                        conJoint.rotation = Quaternion.AngleAxis((float)costTable[goal.I, goal.J].theta, Vector3.up) * conJoint.rotation;
                        alignedConMotion.frameData.Add(conJoint);
                    }
                    else
                    {
                        refIndex.Add(goal.I);
                        alignedRefMotion.frameData.Add(new RootJoint(refMotion.frameData[goal.I]));
                    }
                    costList.Add(costTable[goal.I, goal.J].cost);
                }
            }
            else
            {
                alignedConMotion.frameData.Add(new RootJoint(conMotion.frameData[goal.J]));
                alignedRefMotion.frameData.Add(new RootJoint(refMotion.frameData[goal.I]));
                costList.Add(costTable[goal.I, goal.J].cost);
                conIndex.Add(goal.J);
                refIndex.Add(goal.I);
                break;
            }
            goal = table[goal.preI, goal.preJ];
        }
        alignedConMotion.frameData.Reverse();
        alignedRefMotion.frameData.Reverse();
        alignedConMotion.frames = alignedConMotion.frameData.Count;
        alignedRefMotion.frames = alignedRefMotion.frameData.Count;
        Debug.Log("Warping Done");
    }
    private void forwardPath(int i, int j)
    {
        for (int wi = 1; wi < slopeLimit; wi++)
        {

        }
        for (int wj = 2; wj < slopeLimit; wj++)
        {

        }
    }
    private void BackwardPath(int i, int j)
    {
        int minI = 1, minJ = 1;
        float minCost = float.MaxValue;
        for (int wi = 1; wi <= slopeLimit; wi++)
        {
            if (i - wi < 0)
                continue;
            if (!table[i - wi, j - 1].set)
            {
                BackwardPath(i - wi, j - 1);
            }
            //calculate cost
            float cost = table[i - wi, j - 1].cost;
            for (int ci = 0; ci < wi; ci++)
            {
                cost += costTable[i - ci, j].cost;
            }
            if (minCost > cost)
            {
                minI = wi;
                minJ = 1;
                minCost = cost;
            }
        }
        for (int wj = 2; wj <= slopeLimit; wj++)
        {
            if (j - wj < 0)
                continue;
            if (!table[i - 1, j - wj].set)
            {
                BackwardPath(i - 1, j - wj);
            }
            //calculate cost
            float cost = table[i - 1, j - wj].cost;
            for (int cj = 0; cj < wj; cj++)
            {
                cost += costTable[i, j - cj].cost;
            }
            if (minCost > cost)
            {
                minI = 1;
                minJ = wj;
                minCost = cost;
            }
        }
        table[i, j].I = i;
        table[i, j].J = j;
        table[i, j].preI = i - minI;
        table[i, j].preJ = j - minJ;
        table[i, j].cost = minCost;
        table[i, j].pathLen = table[i - minI, j - minJ].pathLen + Mathf.Max(minI, minJ);
        table[i, j].set = true;
    }
    private Vector3[] GetFrameData(List<Vector3[]> m, int frameIdx)
    {
        if (frameIdx > m.Count - 1)
            return m[m.Count - 1];
        else if (frameIdx < 0)
            return m[0];
        else
            return m[frameIdx];

    }
    private List<Vector3[]> ConstructMotionPointCloud(BVH_Motion.Motion motion)
    {
        List<Vector3[]> motionPointCloud = new List<Vector3[]>();
        JointInfo offsetInfo = new JointInfo();
        ConstructChildInfo(offsetInfo, motion.rootJointInfo);
        for (int i = 0; i < motion.frames; i++)
        {
            List<Vector3> pose = new List<Vector3>();
            Vector3 rootPos = motion.rootJointInfo.offset + motion.frameData[i].position + motion.frameData[i].positionDisplacement;
            offsetInfo.offset = rootPos;
            pose.Add(rootPos);
            SetChildPoint(motion.frameData[i], motion.rootJointInfo, offsetInfo, Quaternion.identity, ref pose);
            motionPointCloud.Add(pose.ToArray());
        }
        return motionPointCloud;
    }
    private void SetChildPoint(BVH_Motion.Joint joint, JointInfo originInfo, JointInfo parentInfo, Quaternion preRotate, ref List<Vector3> pose)
    {
        Quaternion rotate = preRotate * joint.rotation * joint.rotationDisplacement;
        for (int i = 0; i < originInfo.ChildJointsInfo.Count; i++)
        {
            Vector3 dir = rotate * originInfo.ChildJointsInfo[i].offset;
            Vector3 jointPos = parentInfo.offset + dir;
            parentInfo.ChildJointsInfo[i].offset = jointPos;
            pose.Add(jointPos);
            InterpolatePoint(parentInfo.offset, parentInfo.ChildJointsInfo[i].offset, ref pose);
            SetChildPoint(joint.ChildJoints[i], originInfo.ChildJointsInfo[i], parentInfo.ChildJointsInfo[i], rotate, ref pose);
        }
    }

    private void InterpolatePoint(Vector3 sou, Vector3 des, ref List<Vector3> pose)
    {
        for (int i = 1; i <= extraPointNum; i++)
        {
            pose.Add(Vector3.Lerp(sou, des, (float)i / (extraPointNum + 1)));
        }
    }
    private void ConstructChildInfo(JointInfo info, JointInfo data)
    {
        info.ChildJointsInfo = new List<JointInfo>(data.ChildJointsInfo.Count);
        info.name = data.name;
        info.offset = data.offset;
        info.type = data.type;
        for (int i = 0; i < data.ChildJointsInfo.Count; i++)
        {
            info.ChildJointsInfo.Add(new JointInfo());
            info.ChildJointsInfo[i].parentJointInfo = info;
        }
        for (int i = 0; i < info.ChildJointsInfo.Count; i++)
        {
            ConstructChildInfo(info.ChildJointsInfo[i], data.ChildJointsInfo[i]);
        }
    }
}
