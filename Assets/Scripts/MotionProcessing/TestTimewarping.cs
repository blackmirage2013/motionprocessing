﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestTimewarping : MonoBehaviour
{
    public BVH_MotionViewer viewer;
    public BVH_MotionViewer viewer2;
    public TimerPlayer player1;
    public TimerPlayer player2;
    TimeWarping timeWarping = new TimeWarping();
    private void Start()
    {
    }
    public void testFun()
    {
        timeWarping.Init(viewer.m, viewer2.m);
        timeWarping.Do(viewer.m.frames, viewer2.m.frames);
        viewer2.m = timeWarping.alignedRefMotion;
        viewer.m = timeWarping.alignedConMotion;
        // player1.frameCount = player2.frameCount = timeWarping.alignedConMotion.frameData.Count;
    }
}
