﻿using BVH_Motion;
using PathCreation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BVH_Transition_MotionViewer : MonoBehaviour
{
    public UnityEngine.UI.Text mt;
    public static List<BVH_Transition_MotionViewer> allViewers = new List<BVH_Transition_MotionViewer>();
    public BVH_Transition_BodyDisplayer bodyDisplayer;
    public List<BVH_Motion.Motion> ms = new List<BVH_Motion.Motion>();
    public VertexPath oriVertexPath;
    public TransitionFrameUpdate transitionFrameUpdate = new TransitionFrameUpdate();
    public InfoFrameUpdate frameUpdate = new InfoFrameUpdate();
    public PathCreator pathCreator;
    public SplineRender splineRender;
    public int nowFrame = 0;
    public int filterSize = 10;
    public int playingMotionIdx = 0;
    public int nextMotionIdx = 0;
    private BVH_Motion.Motion playingMotion;
    private BVH_Motion.Motion nextMotion;
    // Timer Control
    public bool isPlaying = false; // 是否在播放
    public int rootFrame = 0;
    public int playingFrame = 0;
    public int nextPlayingFrame = 0;
    public int frameCount; // 總幀數
    public float speed = 1f;
    private float frameTime; //一幀的長度
    private float nextPlayTime; //上一次的播放時間
    const float blendLength = 11;
    float blendingFrame = 0;
    public void Init(float _frameTime, int _frameCount)
    {
        frameCount = _frameCount;
        frameTime = _frameTime;
    }
    private void Start()
    {
        ms.Add(BVH_Motion.Motion.Load(Application.streamingAssetsPath + "/coolwalk.bvh"));
        ms.Add(BVH_Motion.Motion.Load(Application.streamingAssetsPath + "/sexywalk.bvh"));
        ms.Add(BVH_Motion.Motion.Load(Application.streamingAssetsPath + "/frighten_run.bvh"));
        playingMotion = ms[playingMotionIdx];
        rotate(ms[2]);
        nextMotion = ms[0];
        SetMotion(ms[0]);
        mt.text = "coolwalk";
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Q))
        {
            nextMotionIdx = 0;
            mt.text = "coolwalk";
        }
        else if (Input.GetKey(KeyCode.W))
        {
            nextMotionIdx = 1;
            mt.text = "sexywalk";
        }
        else if (Input.GetKey(KeyCode.E))
        {
            nextMotionIdx = 2;
            mt.text = "frighten run";
        }
        if (isPlaying)
        {
            if (Time.realtimeSinceStartup > nextPlayTime)
            {
                // At least 1 step
                int step = (int)((Time.realtimeSinceStartup - nextPlayTime) / frameTime) + 1;
                rootFrame = (rootFrame + step) % frameCount;
                playingFrame = (playingFrame + step) % playingMotion.frames;
                nextPlayingFrame = (nextPlayingFrame + step) % nextMotion.frames;
                nextPlayTime = nextPlayTime + step * frameTime;
                if (playingMotionIdx != nextMotionIdx)
                    ChangeMotion();

            }
            float ratio = 1.0f - (nextPlayTime - Time.realtimeSinceStartup) / frameTime;
            float realRootFrame = ((float)(rootFrame) + ratio) % (float)frameCount;
            float realPlayingFrame = ((float)(playingFrame) + ratio) % (float)playingMotion.frameData.Count;
            float realNextPlayingFrame = ((float)(nextPlayingFrame) + ratio) % (float)nextMotion.frameData.Count;
            if (blendingFrame <= 0)
                SetFrame(realRootFrame, realPlayingFrame);
            else
            {
                SetBlendFrame(realRootFrame, realPlayingFrame, realNextPlayingFrame, 1f - blendingFrame / blendLength);
                blendingFrame -= ratio;
                if (blendingFrame <= 0)
                {
                    playingMotion = nextMotion;
                    playingFrame = nextPlayingFrame;
                    playingMotionIdx = nextMotionIdx;
                    if(playingMotionIdx==0)
                        mt.text = "coolwalk";
                    if (playingMotionIdx == 1)
                        mt.text = "sexywalk";
                    if (playingMotionIdx == 2)
                        mt.text = "frighten run";
                    Debug.Log("change");
                }
            }
        }
    }
    void ChangeMotion()
    {
        if (playingFrame == 262 - 5 && playingMotionIdx == 0 && nextMotionIdx == 1)
        {
            nextMotion = ms[nextMotionIdx];
            nextPlayingFrame = 420;
            blendingFrame = blendLength;
        }
        else if (playingFrame == 420 - 5 && playingMotionIdx == 1 && nextMotionIdx == 0)
        {
            nextMotion = ms[nextMotionIdx];
            nextPlayingFrame = 262;
            blendingFrame = blendLength;
        }

        else if (playingFrame == 7 - 5 && playingMotionIdx == 0 && nextMotionIdx == 2)
        {
            nextMotion = ms[nextMotionIdx];
            nextPlayingFrame = 12;
            blendingFrame = blendLength;
        }
        else if (playingFrame == 12 - 5 && playingMotionIdx == 2 && nextMotionIdx == 0)
        {
            nextMotion = ms[nextMotionIdx];
            nextPlayingFrame = 7;
            blendingFrame = blendLength;
        }

        else if (playingFrame == 24 - 5 && playingMotionIdx == 1 && nextMotionIdx == 2)
        {
            nextMotion = ms[nextMotionIdx];
            nextPlayingFrame = 14;
            blendingFrame = blendLength;
        }
        else if (playingFrame == 14 - 5 && playingMotionIdx == 2 && nextMotionIdx == 1)
        {
            nextMotion = ms[nextMotionIdx];
            nextPlayingFrame = 24;
            blendingFrame = blendLength;
        }
        
    }
    public void Play()
    {
        nextPlayTime = Time.realtimeSinceStartup + frameTime;
        isPlaying = true;
    }

    public void Pause()
    {
        isPlaying = false;
    }

    private void Awake()
    {
        allViewers.Add(this);
    }

    List<Vector3> GetFilteredPosition(float positionDistance)
    {
        List<Vector3> positions = new List<Vector3>();
        for (int i = 0; i < ms[0].frameData.Count; ++i)
        {
            Vector3 positionSum = new Vector3();
            float weightSum = 0;
            int halfFilterSize = filterSize / 2;

            if (i + halfFilterSize >= ms[0].frameData.Count)
                halfFilterSize = ms[0].frameData.Count - i;
            if (i - halfFilterSize < 0)
                halfFilterSize = i;

            for (int j = -halfFilterSize; j <= halfFilterSize; ++j)
            {
                if (i + j < 0 || i + j >= ms[0].frameData.Count)
                    continue;
                float weight = Mathf.Exp(-j ^ 2);
                positionSum += ms[0].frameData[i + j].position * weight;
                weightSum += weight;
            }
            Vector3 newPosition = positionSum / weightSum;
            newPosition.y = 1;
            if (positions.Count == 0 || (positions[positions.Count - 1] - newPosition).magnitude > positionDistance || i == ms[0].frameData.Count - 1)
                positions.Add(newPosition);
        }
        return positions;
    }
    public void SetMotion(BVH_Motion.Motion tmp)
    {
        if (filterSize < 1) filterSize = 1;

        BezierPath oriBezierPath = new BezierPath(GetFilteredPosition(20), false, PathSpace.xyz);
        oriVertexPath = new VertexPath(oriBezierPath, pathCreator.transform);

        SelectMotionToMotionView();
        foreach (var m in ms)
            Normalize(m);
        SetFrame(0, 0);
    }
    public float ConvertVecToAngle(Vector2 v)
    {
        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public void FollowPath(BezierPath bezierPath)
    {
        VertexPath newVertexPath = new VertexPath(bezierPath, pathCreator.transform);
        float oriLength = oriVertexPath.length;
        float newLength = newVertexPath.length;
        for (int i = 0; i < ms[0].frameData.Count; i++)
        {
            float t = ((float)i + 1) / ((float)ms[0].frameData.Count);

            // Position
            float oriDis = oriVertexPath.GetClosestDistanceAlongPath(ms[0].frameData[i].position);
            float newDis = oriDis / oriLength * newLength;
            Vector3 oriPoint = ms[0].frameData[i].position;
            Vector3 point = newVertexPath.GetPointAtDistance(newDis);
            Vector3 positionDisplacement = point - oriPoint;
            positionDisplacement.y = 0;
            ms[0].frameData[i].positionDisplacement = positionDisplacement;

            // Rotation
            Quaternion oriRotation = oriVertexPath.GetRotation(t);
            Vector3 oriForward = oriRotation * Vector3.forward;
            Quaternion newRotation = newVertexPath.GetRotation(t);
            Vector3 newForward = newRotation * Vector3.forward;

            float oriAngle = ConvertVecToAngle(new Vector2(oriForward.x, oriForward.z));
            float newAngle = ConvertVecToAngle(new Vector2(newForward.x, newForward.z));

            ms[0].frameData[i].rotationDisplacement = Quaternion.Euler(0, oriAngle - newAngle, 0);
        }
    }

    public void SelectMotionToMotionView()
    {
        List<Vector3> positions = GetFilteredPosition(20);
        Init(ms[0].frames_time, ms[0].frameData.Count);
        bodyDisplayer.Init(ms[0].rootJointInfo);
        BezierPath bezierPath = new BezierPath(positions, false, PathSpace.xyz);

        pathCreator.bezierPath = bezierPath;
        pathCreator.TriggerPathUpdate();
        // splineRender.PathUpdated();
    }

    public void SetFrame(float _rootframe, float _playingframe)
    {
        nowFrame = (int)_rootframe;
        float rf = _rootframe % ms[0].frameData.Count;
        float pf = _playingframe % playingMotion.frameData.Count;
        transitionFrameUpdate.Invoke(ms[0].GetFrame(rf), playingMotion.GetFrame(pf), playingMotion.rootJointInfo);
    }
    public void SetBlendFrame(float _rootframe, float _playingframe, float _next_playingframe, float _ratio)
    {
        nowFrame = (int)_rootframe;
        float rf = _rootframe % ms[0].frameData.Count;
        float pf = _playingframe % playingMotion.frameData.Count;
        float nf = _next_playingframe % nextMotion.frameData.Count;
        transitionFrameUpdate.Invoke(ms[0].GetFrame(rf), playingMotion.GetFrame((int)pf, (int)nf, _ratio, nextMotion), playingMotion.rootJointInfo);
    }

    public void SetFrame(float frame, BVH_Motion.Motion m)
    {
        nowFrame = (int)frame;
        float f = frame % m.frameData.Count;
        frameUpdate.Invoke(m.GetFrame(f), playingMotion.rootJointInfo);
    }
    public void rotate(BVH_Motion.Motion m)
    {
        for (int i = 0; i < m.frameData.Count; ++i)
        {
            m.frameData[i].rotation = m.frameData[i].rotation * Quaternion.AngleAxis(-90f, Vector3.up);
        }
    }
    public void Normalize(BVH_Motion.Motion m)
    {
        /* Normalize Motion with BodyDisplayer
         * Constraints that must be satisfied:
         * 1. The lowest position must be on ground.
         */
        int originalFrame = nowFrame;

        SetFrame(0, m);
        Vector3[] boundingBox = bodyDisplayer.GetBoundingBox();
        for (int i = 1; i < m.frameData.Count; ++i)
        {
            SetFrame(i, m); ;
            Vector3[] boundingBoxTemp = bodyDisplayer.GetBoundingBox();
            for (int j = 0; j < 3; ++j)
            {
                if (boundingBoxTemp[0][j] < boundingBox[0][j])
                    boundingBox[0][j] = boundingBoxTemp[0][j];
                if (boundingBoxTemp[1][j] > boundingBox[1][j])
                    boundingBox[1][j] = boundingBoxTemp[1][j];
            }
        }
        SetFrame(originalFrame, m); ;

        float lowestY = boundingBox[0].y;
        for (int i = 0; i < m.frameData.Count; ++i)
        {
            m.frameData[i].position += new Vector3(0, -lowestY, 0);
        }
    }
}
