﻿using BVH_Motion;
using PathCreation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(PathCreator))]
public class BVH_MotionViewer : MonoBehaviour
{
    public static List<BVH_MotionViewer> allViewers = new List<BVH_MotionViewer>();
    public BVH_BodyDisplayer bodyDisplayer;
    public BVH_CharacterDisplayer characterDisplayer;
    public BVH_Motion.Motion m;
    public VertexPath oriVertexPath;
    public FrameUpdate frameUpdate = new FrameUpdate();
    public PathCreator pathCreator;
    public SplineRender splineRender;
    public bool interpolation;
    public int nowFrame = 0;
    public int filterSize = 10;

    // Timer Control
    public bool isPlaying = false; // 是否在播放
    public int frame = 0; //目前播放的Index
    public int frameCount; // 總幀數
    public float speed = 1f;
    private float frameTime; //一幀的長度
    private float nextPlayTime; //上一次的播放時間
    public void Init(float _frameTime, int _frameCount)
    {
        frameCount = _frameCount;
        frameTime = _frameTime;
        frame = 0;
    }

    private void Update()
    {
        if (isPlaying)
        {
            if (Time.realtimeSinceStartup > nextPlayTime)
            {
                // At least 1 step
                int step = (int)((Time.realtimeSinceStartup - nextPlayTime) / frameTime) + 1;
                frame = (frame + step) % frameCount;
                nextPlayTime = nextPlayTime + step * frameTime;
            }
            float ratio = 1.0f - (nextPlayTime - Time.realtimeSinceStartup) / frameTime;
            float realFrame = ((float)(frame) + ratio) % (float)frameCount;
            SetFrame(realFrame);
        }
    }
    public void Play()
    {
        nextPlayTime = Time.realtimeSinceStartup + frameTime;
        isPlaying = true;
    }

    public void Pause()
    {
        isPlaying = false;
    }


    private void Awake()
    {
        allViewers.Add(this);
    }

    List<Vector3> GetFilteredPosition(float positionDistance)
    {
        List<Vector3> positions = new List<Vector3>();
        for (int i = 0; i < m.frameData.Count; ++i)
        {
            Vector3 positionSum = new Vector3();
            float weightSum = 0;
            int halfFilterSize = filterSize / 2;

            if (i + halfFilterSize >= m.frameData.Count)
                halfFilterSize = m.frameData.Count - i - 1;
            if (i - halfFilterSize < 0)
                halfFilterSize = i;

            for (int j = -halfFilterSize; j <= halfFilterSize; ++j)
            {
                float weight = Mathf.Exp(-j ^ 2);
                positionSum += m.frameData[i + j].position * weight;
                weightSum += weight;
            }
            Vector3 newPosition = positionSum / weightSum;
            newPosition.y = 1;
            if (positions.Count == 0 || (positions[positions.Count - 1] - newPosition).magnitude > positionDistance || i == m.frameData.Count - 1)
                positions.Add(newPosition);
        }
        return positions;
    }
    public void SetMotion(BVH_Motion.Motion tmp)
    {
        m = tmp;
        if (filterSize < 1) filterSize = 1;

        SelectMotionToMotionView();
        SetFrame(0);
    }
    public float ConvertVecToAngle(Vector2 v)
    {
        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public void FollowPath(BezierPath bezierPath)
    {
        VertexPath newVertexPath = new VertexPath(bezierPath, pathCreator.transform);
        float oriLength = oriVertexPath.length;
        float newLength = newVertexPath.length;
        for (int i = 0; i < m.frameData.Count; i++)
        {
            float t = ((float)i + 1) / ((float)m.frameData.Count);

            // Position
            Vector3 oriPoint = oriVertexPath.GetPointAtTime(t);
            Vector3 point = newVertexPath.GetPointAtTime(t);
            Vector3 positionDisplacement = point - oriPoint;
            positionDisplacement.y = 0;
            m.frameData[i].positionDisplacement = positionDisplacement;

            // Rotation
            Quaternion oriRotation = oriVertexPath.GetRotation(t);
            Vector3 oriForward = oriRotation * Vector3.forward;
            Quaternion newRotation = newVertexPath.GetRotation(t);
            Vector3 newForward = newRotation * Vector3.forward;

            float oriAngle = ConvertVecToAngle(new Vector2(oriForward.x, oriForward.z));
            float newAngle = ConvertVecToAngle(new Vector2(newForward.x, newForward.z));

            m.frameData[i].rotationDisplacement = Quaternion.Euler(0, oriAngle - newAngle, 0);
        }
    }
    public void solveIK()
    {
        GetComponent<IKSolver>().solveIK();
    }
    public void SelectMotionToMotionView()
    {
        Init(m.frames_time, m.frameData.Count);
        bodyDisplayer.Init(m.rootJointInfo);
        Normalize();

        GetComponent<IKSolver>().findNearGroundJoint();

        BezierPath oriBezierPath = new BezierPath(GetFilteredPosition(20), false, PathSpace.xyz);
        oriVertexPath = new VertexPath(oriBezierPath, pathCreator.transform);
        List<Vector3> positions = GetFilteredPosition(20);
        BezierPath bezierPath = new BezierPath(positions, false, PathSpace.xyz);

        pathCreator.bezierPath = bezierPath;
        pathCreator.TriggerPathUpdate();
        characterDisplayer?.Init(m.rootJointInfo);
        // splineRender.PathUpdated();
    }

    public void SetFrame(float frame)
    {
        nowFrame = (int)frame;
        if (nowFrame != frame && interpolation)
        {
            float f = frame % m.frameData.Count;
            frameUpdate.Invoke(m.GetFrame(f));
        }
        else
        {
            frameUpdate.Invoke(m.frameData[((int)frame) % m.frameData.Count]);
        }
    }


    public void Normalize()
    {
        /* Normalize Motion with BodyDisplayer
         * Constraints that must be satisfied:
         * 1. The lowest position must be on ground.
         * 
         * Constraints for convenience:
         * 1. No glitch between frames
         */
        int originalFrame = nowFrame;

        //// Find all joints
        //List<BVH_Motion.Joint>[] frameJoints = new List<BVH_Motion.Joint>[m.frameData.Count];
        //for (int i = 0; i < m.frameData.Count; ++i)
        //{
        //    SetFrame(i);
        //    List<BVH_Motion.Joint> allJoints = m.frameData[i].GetAllChildJoints();
        //    allJoints.Insert(0, m.frameData[i]);
        //    frameJoints[i] = allJoints;
        //}

        //// Median filter per joint
        //for(int i = 0; i < frameJoints[0].Count; ++i)
        //{
        //    for(int j = 0; j < frameJoints.Length; ++j)
        //    {
        //        Vector3 originalRotation = frameJoints[j][i].rotation.eulerAngles;

        //        int halfFilterSize = filterSize / 2;
        //        if (j + halfFilterSize >= m.frameData.Count)
        //            halfFilterSize = m.frameData.Count - j - 1;
        //        if (j - halfFilterSize < 0)
        //            halfFilterSize = j;

        //        List<List<float>> allRotation = new List<List<float>>();
        //        for (int k = 0; k < 3; ++k)
        //            allRotation.Add(new List<float>(halfFilterSize * 2 + 1));

        //        for (int k = -halfFilterSize; k <= halfFilterSize; ++k)
        //        {
        //            Vector3 rotation = frameJoints[j + k][i].rotation.eulerAngles;
        //            for (int l = 0; l < 3; ++l) {
        //                while (rotation[l] - originalRotation[l] >= 180) rotation[l] -= 360;
        //                while (rotation[l] - originalRotation[l] <= -180) rotation[l] += 360;
        //                allRotation[l].Add(rotation[l]);
        //            }
        //        }
        //        // Median filter
        //        for (int l = 0; l < 3; ++l)
        //            allRotation[l].Sort();

        //        frameJoints[j][i].rotation = Quaternion.Euler(allRotation[0][halfFilterSize], allRotation[1][halfFilterSize], allRotation[2][halfFilterSize]);
        //    }
        //}

        // To ground
        List<float> lowestYs = new List<float>();
        for (int i = 0; i < m.frameData.Count; ++i)
        {
            SetFrame(i);
            Vector3[] boundingBoxTemp = bodyDisplayer.GetBoundingBox();
            lowestYs.Add(boundingBoxTemp[0][1]);
        }
        SetFrame(originalFrame);

        lowestYs.Sort();
        float lowestY = lowestYs[lowestYs.Count / 4];
        for (int i = 0; i < m.frameData.Count; ++i)
        {
            m.frameData[i].position += new Vector3(0, -lowestY, 0);
        }
    }
}
