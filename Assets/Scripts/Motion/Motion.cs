﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Reflection;

namespace BVH_Motion
{
    public static class Folder
    {
        public static string DataPath
        {
            get
            {
                string result = Path.Combine(Directory.GetCurrentDirectory(), "ProjectData");
                if (Directory.Exists(result))
                {
                    return result;
                }
                else
                {
                    return Directory.GetCurrentDirectory();
                }
            }
        }

        public static string Motions_Folder
        {
            get
            {
                string result = Path.Combine(DataPath, "Motions");
                if (Directory.Exists(result))
                {
                    return result;
                }
                else
                {
                    return Directory.GetCurrentDirectory();
                }
            }
        }
    }

    public class Define
    {
        [Serializable]
        public enum BVH_RotateType
        {
            XYZ,
            XZY,
            YXZ,
            YZX,
            ZXY,
            ZYX
        }

        public static Dictionary<string, BVH_RotateType> String2RotateType = new Dictionary<string, BVH_RotateType>()
        {
            {"Xrotation Yrotation Zrotation", BVH_RotateType.XYZ},
            {"Xrotation Zrotation Yrotation", BVH_RotateType.XZY},
            {"Yrotation Xrotation Zrotation", BVH_RotateType.YXZ},
            {"Yrotation Zrotation Xrotation", BVH_RotateType.YZX},
            {"Zrotation Xrotation Yrotation", BVH_RotateType.ZXY},
            {"Zrotation Yrotation Xrotation", BVH_RotateType.ZYX}
        };
        private static float wrapAngle(float a)
        {
            if (a > 180f)
            {
                return a - 360f;
            }
            if (a < -180f)
            {
                return 360f + a;
            }
            return a;
        }

        public static Quaternion Euler2Quaternion(float[] angles, BVH_RotateType type)
        {
            int[] rotateOrder = new int[3] { 0, 1, 2 };
            Vector3 axisX = Vector3.right;
            Vector3 axisY = Vector3.up;
            Vector3 axisZ = Vector3.forward;
            Quaternion qt = Quaternion.identity;
            switch (type)//switch (比對的運算式)
            {
                case BVH_RotateType.XYZ:
                    qt = Quaternion.AngleAxis(angles[0], axisX) * Quaternion.AngleAxis(angles[1], axisY) * Quaternion.AngleAxis(angles[2], axisZ);
                    break;
                case BVH_RotateType.XZY:
                    qt = Quaternion.AngleAxis(angles[0], axisX) * Quaternion.AngleAxis(angles[1], axisZ) * Quaternion.AngleAxis(angles[2], axisY);
                    break;
                case BVH_RotateType.YXZ:
                    qt = Quaternion.AngleAxis(angles[0], axisY) * Quaternion.AngleAxis(angles[1], axisX) * Quaternion.AngleAxis(angles[2], axisZ);
                    break;
                case BVH_RotateType.YZX:
                    qt = Quaternion.AngleAxis(angles[0], axisY) * Quaternion.AngleAxis(angles[1], axisZ) * Quaternion.AngleAxis(angles[2], axisX);
                    break;
                case BVH_RotateType.ZXY:
                    qt = Quaternion.AngleAxis(angles[0], axisZ) * Quaternion.AngleAxis(angles[1], axisX) * Quaternion.AngleAxis(angles[2], axisY);
                    break;
                case BVH_RotateType.ZYX:
                    qt = Quaternion.AngleAxis(angles[0], axisZ) * Quaternion.AngleAxis(angles[1], axisY) * Quaternion.AngleAxis(angles[2], axisX);
                    break;
            }
            return new Quaternion(-qt.x, qt.y, qt.z, -qt.w);
        }
    }

    [Serializable]
    public class JointInfo
    {
        [SerializeField]
        public List<JointInfo> ChildJointsInfo;
        public string name;
        public Vector3 offset;
        [SerializeField]
        public Define.BVH_RotateType type;
        public JointInfo parentJointInfo;
        public JointInfo() { ChildJointsInfo = new List<JointInfo>(); }
        public static JointInfo Clone(JointInfo source)
        {
            JointInfo jInfo = new JointInfo();
            jInfo.name = source.name;
            jInfo.offset = source.offset;
            jInfo.type = source.type;
            jInfo.parentJointInfo = jInfo;
            jInfo.ChildJointsInfo = new List<JointInfo>(new JointInfo[source.ChildJointsInfo.Count]);
            for (int i = 0; i < source.ChildJointsInfo.Count; i++)
            {
                jInfo.ChildJointsInfo[i] = new JointInfo();
                Clone(source.ChildJointsInfo[i], jInfo.ChildJointsInfo[i], jInfo);
            }
            return jInfo;
        }
        private static void Clone(JointInfo source, JointInfo target, JointInfo targetParent)
        {
            target.name = source.name;
            target.offset = source.offset;
            target.type = source.type;
            target.parentJointInfo = targetParent;
            target.ChildJointsInfo = new List<JointInfo>(new JointInfo[source.ChildJointsInfo.Count]);
            for (int i = 0; i < source.ChildJointsInfo.Count; i++)
            {
                target.ChildJointsInfo[i] = new JointInfo();
                Clone(source.ChildJointsInfo[i], target.ChildJointsInfo[i], target);
            }
        }
        public JointInfo(string n, JointInfo p)
        {
            name = n;
            parentJointInfo = p;
            ChildJointsInfo = new List<JointInfo>();
        }
        public int JointCount(int count)
        {
            count++;
            for (int i = 0; i < ChildJointsInfo.Count; i++)
            {
                count = ChildJointsInfo[i].JointCount(count);
            }
            return count;
        }
    }

    [Serializable]
    public class RootJoint : Joint
    {
        public Vector3 position;
        public Vector3 positionDisplacement = new Vector3();
        public static RootJoint Blend(RootJoint refJoint, RootJoint conJoint, float ratio)
        {
            RootJoint rootJoint = new RootJoint();
            rootJoint.position = Vector3.Lerp(refJoint.position, conJoint.position, ratio);
            rootJoint.positionDisplacement = Vector3.Lerp(refJoint.positionDisplacement, conJoint.positionDisplacement, ratio);
            rootJoint.rotation = Quaternion.Lerp(refJoint.rotation, conJoint.rotation, ratio);
            rootJoint.rotationDisplacement = Quaternion.Lerp(refJoint.rotationDisplacement, conJoint.rotationDisplacement, ratio);
            FrameInterpolation(refJoint, conJoint, ratio, rootJoint);
            return rootJoint;
        }

        // 上一幀 下一幀 要複製上的新動作
        public static void FrameInterpolation(Joint lastJoint, Joint nextJoint, float ratio, Joint newJoint)
        {
            newJoint.ChildJoints = new Joint[lastJoint.ChildJoints.Length];
            for (int i = 0; i < lastJoint.ChildJoints.Length; i++)
            {
                newJoint.ChildJoints[i] = new Joint();
                newJoint.ChildJoints[i].rotation = Quaternion.Lerp(lastJoint.ChildJoints[i].rotation, nextJoint.ChildJoints[i].rotation, ratio);
                newJoint.ChildJoints[i].rotationDisplacement = Quaternion.Lerp(lastJoint.ChildJoints[i].rotationDisplacement, nextJoint.ChildJoints[i].rotationDisplacement, ratio);
                FrameInterpolation(lastJoint.ChildJoints[i], nextJoint.ChildJoints[i], ratio, newJoint.ChildJoints[i]);
            }
        }



        public static RootJoint Clone(RootJoint source)
        {
            RootJoint rj = new RootJoint();
            rj.position = source.position;
            rj.positionDisplacement = source.positionDisplacement;
            rj.rotation = source.rotation;
            rj.rotationDisplacement = source.rotationDisplacement;
            rj.ChildJoints = new Joint[source.ChildJoints.Length];
            for (int i = 0; i < source.ChildJoints.Length; i++)
            {
                rj.ChildJoints[i] = Clone(source.ChildJoints[i]);
            }
            return rj;
        }
        private static Joint Clone(Joint source)
        {
            Joint target = new Joint();
            target.rotation = source.rotation;
            target.rotationDisplacement = source.rotationDisplacement;
            target.ChildJoints = new Joint[source.ChildJoints.Length];
            for (int i = 0; i < source.ChildJoints.Length; i++)
            {
                target.ChildJoints[i] = Clone(source.ChildJoints[i]);
            }
            return target;
        }

        public static void ParserFrameData(Joint curJoint, JointInfo curInfo, Queue<string> frame)
        {
            curJoint.ChildJoints = new Joint[curInfo.ChildJointsInfo.Count];
            for (int i = 0; i < curJoint.ChildJoints.Length; i++)
            {
                if (curInfo.ChildJointsInfo[i].name.Contains("EndSite"))
                {
                    curJoint.ChildJoints[i] = new Joint();
                    curJoint.ChildJoints[i].parent = curJoint;
                    curJoint.ChildJoints[i].rotation = Quaternion.identity;
                    curJoint.ChildJoints[i].ChildJoints = new Joint[0];
                }
                else
                {
                    curJoint.ChildJoints[i] = new Joint();
                    curJoint.ChildJoints[i].parent = curJoint;
                    float[] angles = new float[] { float.Parse(frame.Dequeue()), float.Parse(frame.Dequeue()), float.Parse(frame.Dequeue()) };
                    curJoint.ChildJoints[i].rotation = Define.Euler2Quaternion(angles, curInfo.ChildJointsInfo[i].type);
                    ParserFrameData(curJoint.ChildJoints[i], curInfo.ChildJointsInfo[i], frame);
                }
            }
        }
        public RootJoint() { }
        public RootJoint(RootJoint _rootJoint)
        {
            position = _rootJoint.position;
            positionDisplacement = _rootJoint.positionDisplacement;
            rotation = _rootJoint.rotation;
            rotationDisplacement = _rootJoint.rotationDisplacement;
            ChildJoints = new Joint[_rootJoint.ChildJoints.Length];
            for (int i = 0; i < _rootJoint.ChildJoints.Length; i++)
            {
                ChildJoints[i] = new Joint(_rootJoint.ChildJoints[i]);
                ChildJoints[i].parent = this;
            }
        }
    }

    [Serializable]
    public class Joint
    {
        [SerializeField]
        public Joint[] ChildJoints;
        public Joint parent = null;
        public Quaternion rotation;
        public Quaternion rotationDisplacement = Quaternion.identity;
        public Joint() { }
        public Joint(Joint _joint)
        {
            rotation = _joint.rotation;
            rotationDisplacement = _joint.rotationDisplacement;
            ChildJoints = new Joint[_joint.ChildJoints.Length];
            for (int i = 0; i < _joint.ChildJoints.Length; i++)
            {
                ChildJoints[i] = new Joint(_joint.ChildJoints[i]);
                ChildJoints[i].parent = this;
            }
        }
        public List<Joint> GetAllChildJoints()
        {
            List<Joint> allJoints = new List<Joint>();
            for (int i = 0; i < ChildJoints.Length; ++i)
            {
                allJoints.Add(ChildJoints[i]);
                allJoints.AddRange(ChildJoints[i].GetAllChildJoints());
            }
            return allJoints;
        }
    }

    [Serializable]
    public class Motion
    {
        [SerializeField]
        public JointInfo rootJointInfo;
        [SerializeField]
        public List<RootJoint> frameData = new List<RootJoint>();

        public int frames;
        public float frames_time;
        public static string MergeSpace(string str)
        {
            return new System.Text.RegularExpressions.Regex("[\\s]+").Replace(str, " ");
        }

        public RootJoint GetFrame(float frame)
        {
            RootJoint rootJoint = new RootJoint();
            int lastFrame = (int)frame;
            int nextFrame = (lastFrame + 1) % frameData.Count;
            float ratio = frame - lastFrame;
            rootJoint.position = Vector3.Lerp(frameData[lastFrame].position, frameData[nextFrame].position, ratio);
            rootJoint.positionDisplacement = Vector3.Lerp(frameData[lastFrame].positionDisplacement, frameData[nextFrame].positionDisplacement, ratio);
            rootJoint.rotation = Quaternion.Lerp(frameData[lastFrame].rotation, frameData[nextFrame].rotation, ratio);
            rootJoint.rotationDisplacement = Quaternion.Lerp(frameData[lastFrame].rotationDisplacement, frameData[nextFrame].rotationDisplacement, ratio);
            FrameInterpolation(frameData[lastFrame], frameData[nextFrame], ratio, rootJoint);
            return rootJoint;
        }
        public RootJoint GetFrame(int frame,int pframe,float ratio, BVH_Motion.Motion pmotion)
        {
            RootJoint rootJoint = new RootJoint();
            rootJoint.position = Vector3.Lerp(frameData[frame].position, pmotion.frameData[pframe].position, ratio);
            rootJoint.positionDisplacement = Vector3.Lerp(frameData[frame].positionDisplacement, pmotion.frameData[pframe].positionDisplacement, ratio);
            rootJoint.rotation = Quaternion.Lerp(frameData[frame].rotation, pmotion.frameData[pframe].rotation, ratio);
            rootJoint.rotationDisplacement = Quaternion.Lerp(frameData[frame].rotationDisplacement, pmotion.frameData[pframe].rotationDisplacement, ratio);
            FrameInterpolation(frameData[frame], pmotion.frameData[pframe], ratio, rootJoint);
            return rootJoint;
        }

        // 上一幀 下一幀 要複製上的新動作
        public void FrameInterpolation(Joint lastJoint, Joint nextJoint, float ratio, Joint newJoint)
        {
            newJoint.ChildJoints = new Joint[lastJoint.ChildJoints.Length];
            for (int i = 0; i < lastJoint.ChildJoints.Length; i++)
            {
                newJoint.ChildJoints[i] = new Joint();
                newJoint.ChildJoints[i].parent = newJoint;
                newJoint.ChildJoints[i].rotation = Quaternion.Lerp(lastJoint.ChildJoints[i].rotation, nextJoint.ChildJoints[i].rotation, ratio);
                // newJoint.ChildJoints[i].rotationDisplacement = Quaternion.Lerp(lastJoint.ChildJoints[i].rotationDisplacement, nextJoint.ChildJoints[i].rotationDisplacement, ratio);
                FrameInterpolation(lastJoint.ChildJoints[i], nextJoint.ChildJoints[i], ratio, newJoint.ChildJoints[i]);
            }
        }
        public Motion() { }
        public Motion(Motion _motion)
        {
            frames = _motion.frames;
            frames_time = _motion.frames_time;
            frameData = new List<RootJoint>();
            rootJointInfo = new JointInfo();
        }


        public static Motion Load(string filePath)
        {
            string line = "Init";
            int lineNum = 0;
            try
            {
                Motion m = new Motion();
                StreamReader reader = new StreamReader(filePath);
                line = reader.ReadLine();
                lineNum++;
                JointInfo curJointInfo = new JointInfo("temporary", null); // 假定
                int jointCount = 0;
                int endSiteIndex = 0;
                while (line != null)
                {
                    line = MergeSpace(line.Replace("\t", " ")); // 防止有兩種間距方式
                    if (line.Length == 0)
                    {
                        // Do nothing
                    }
                    else if (line.Contains("HIERARCHY"))
                    {
                        // Do nothing
                    }
                    else if (line.Contains("MOTION"))
                    {
                        // Do nothing
                    }
                    else if (line.Contains("{")) // child
                    {
                        // Do nothing
                    }
                    else if (line.Contains("}")) // sibling
                    {
                        curJointInfo = curJointInfo.parentJointInfo;
                    }
                    else if (line.Contains("ROOT"))
                    {
                        string rootName = line.Replace("ROOT", "").Trim(' ');
                        m.rootJointInfo = new JointInfo(rootName, null);
                        m.rootJointInfo.parentJointInfo = m.rootJointInfo; // 把Root的Parent設為自己
                        m.rootJointInfo.offset = Vector3.zero;
                        m.rootJointInfo.type = Define.BVH_RotateType.XYZ;
                        curJointInfo = m.rootJointInfo;
                        jointCount++;
                    }
                    else if (line.Contains("End Site"))
                    {
                        JointInfo newJointInfo = new JointInfo("EndSite" + endSiteIndex.ToString(), curJointInfo);
                        curJointInfo.ChildJointsInfo.Add(newJointInfo);
                        curJointInfo = newJointInfo;
                        endSiteIndex++;
                        jointCount++;
                    }
                    else if (line.Contains("JOINT"))
                    {
                        string jointName = line.Replace("JOINT", "").Trim(' ');
                        JointInfo newJointInfo = new JointInfo(jointName, curJointInfo);
                        curJointInfo.ChildJointsInfo.Add(newJointInfo);
                        curJointInfo = newJointInfo;
                        jointCount++;
                    }
                    else if (line.Contains("OFFSET"))
                    {
                        // 移除頭尾空白及Tab、OFFSET字串
                        string offsetString = line.Replace("OFFSET", "").Trim(' ');
                        string[] offsetsInfo = offsetString.Split(' ');
                        // Blender to Unity C
                        curJointInfo.offset = new Vector3(-float.Parse(offsetsInfo[0]), float.Parse(offsetsInfo[1]), float.Parse(offsetsInfo[2]));
                    }
                    else if (line.Contains("CHANNELS"))
                    {
                        // 移除頭尾空白及Tab、OFFSET字串
                        string channelsString = line.Replace("CHANNELS", "").Trim(' ');
                        string[] offsets = channelsString.Split(' ');
                        int channelNum = int.Parse(offsets[0]);
                        if (channelNum == 3)
                        {
                            curJointInfo.type = Define.String2RotateType[offsets[1] + " " + offsets[2] + " " + offsets[3]];
                        }
                        else if (channelNum == 6) // TODO joint position attributes
                        {
                            curJointInfo.type = Define.String2RotateType[offsets[4] + " " + offsets[5] + " " + offsets[6]];
                        }
                        else
                        {
                            throw new Exception();
                        }
                    }
                    else if (line.Contains("Frames:"))
                    {
                        string frames = line.Replace("Frames:", "").Trim(' ');
                        m.frames = int.Parse(frames);
                    }
                    else if (line.Contains("Frame Time:"))
                    {
                        string frameTime = line.Replace("Frame Time:", "").Trim(' ');
                        m.frames_time = float.Parse(frameTime);
                    }
                    else
                    {
                        string[] motionData = line.TrimStart(' ').Trim(' ').Split(' ');
                        Queue<string> frame = new Queue<string>(motionData);
                        RootJoint root = new RootJoint();
                        root.position = new Vector3(-float.Parse(frame.Dequeue()), float.Parse(frame.Dequeue()), float.Parse(frame.Dequeue()));
                        float[] rootAngle = new float[] { float.Parse(frame.Dequeue()), float.Parse(frame.Dequeue()), float.Parse(frame.Dequeue()) };
                        // Blender 坐標系到Unity坐標系(左右手互換)

                        root.rotation = Define.Euler2Quaternion(rootAngle, m.rootJointInfo.type);
                        RootJoint.ParserFrameData(root, m.rootJointInfo, frame);
                        m.frameData.Add(root);
                    }
                    line = reader.ReadLine();
                    lineNum++;
                }
                Debug.Log("Joint num " + jointCount.ToString());
                return m;
            }
            catch (Exception)
            {
                Debug.Log("Error line(line num " + lineNum.ToString() + "): + line");
                return null;
            }
        }
    }
}


