﻿using BVH_Motion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BVH_CharacterDisplayer : MonoBehaviour
{
    public BVH_BodyDisplayer bodyDisplayer;
    public CharacterMap target; // 會自動初始化 pose(T/A...)
    public Dictionary<string, Transform> mapping;
    public Dictionary<string, Vector3> initDir;
    public Dictionary<string, Quaternion> OriRot;
    private bool initSuccess = false;

    public void Init(JointInfo rootInfo)
    {
        if (rootInfo.JointCount(0) != 81)
        {
            mapping = new Dictionary<string, Transform>();
            OriRot = new Dictionary<string, Quaternion>();
            initDir = new Dictionary<string, Vector3>();
            TransformMap(rootInfo);
            if (mapping.Count != 18)
            {
                Debug.Log(mapping.Count);
                initSuccess = false;
                gameObject.SetActive(false);
                DialogManager.instance.NewDialog("錯誤提示", "骨架無法Map到角色上");
                return;
            }
            foreach (KeyValuePair<string, Transform> item in mapping)
            {
                OriRot.Add(item.Key, item.Value.rotation); // T pose 時的基本旋轉
            }
            ResetPose();
            foreach (KeyValuePair<string, Transform> item in mapping)
            {
                OriRot[item.Key] = item.Value.rotation; // T pose 時的基本旋轉
            }

            Vector3[] characterBoxTemp = GetBoundingBox();
            Vector3[] boundingBoxTemp = bodyDisplayer.GetBoundingBox();
            Debug.Log((boundingBoxTemp[1][1] - boundingBoxTemp[0][1]));
            Debug.Log((characterBoxTemp[1].y - characterBoxTemp[0].y));
            target.gameObject.transform.localScale = Vector3.one * (boundingBoxTemp[1][1] - boundingBoxTemp[0][1]) / (characterBoxTemp[1][1] - characterBoxTemp[0][1]);
            initSuccess = true;
            gameObject.SetActive(false);
        }
        else
        {
            initSuccess = false;
            gameObject.SetActive(false);
        }
    }
    public Vector3[] GetBoundingBox()
    {
        Vector3[] minmaxPos = new Vector3[2];
        foreach (KeyValuePair<string, Transform> item in mapping)
        {
            Vector3 p = item.Value.position;
            for (int i = 0; i < 3; ++i)
            {
                if (p[i] < minmaxPos[0][i])
                    minmaxPos[0][i] = p[i];
                if (p[i] > minmaxPos[1][i])
                    minmaxPos[1][i] = p[i];
            }
        }
        return minmaxPos;
    }

    (bool, Quaternion) DcmFromAxis(Vector3 xDir, Vector3 yDir, Vector3 zDir, string order)
    {
        //xDir.x *= -1;
        //yDir.x *= -1;
        //zDir.x *= -1;
        Dictionary<string, Vector3> axis = new Dictionary<string, Vector3>()
        {
            { "x", xDir},
            { "y", yDir},
            { "z", zDir}
        };

        string name = "xyz";
        int a, b, c, d;
        if (order == "zyx")
        {
            a = 2;
            b = 0;
            c = 1;
            d = 2;
        }
        else
        {
            a = 0;
            b = 1;
            c = 2;
            d = 0;

        }
        axis[order[0].ToString()] = axis[order[0].ToString()].normalized;
        axis[order[1].ToString()] = Vector3.Cross(axis[name[a].ToString()], axis[name[b].ToString()]).normalized;
        axis[order[2].ToString()] = Vector3.Cross(axis[name[c].ToString()], axis[name[d].ToString()]).normalized;
        Vector3 column2_t = new Vector3(axis["y"].x, axis["y"].y, axis["y"].z);
        Vector3 column3_t = new Vector3(axis["z"].x, axis["z"].y, axis["z"].z);
        if (column2_t.magnitude > 0.000001)
            return (true, Quaternion.LookRotation(column2_t, column3_t));
        else
            return (false, Quaternion.identity);
    }

    // 根據JointInfo 初始化Character動作(只初始化上半身!)
    public void ResetPose() // Reset pose to T or A pose
    {
        // RShoulder
        {
            Vector3 x_dir = -initDir[RightElbow];
            Vector3 y_dir = -initDir[Neck];
            Vector3 yd2 = -initDir[RightShoulder]; 
            Vector3 yCross = Vector3.Cross(yd2, y_dir).normalized * 0.5f;

            Vector3 z_dir = Vector3.zero;
            string order = "xzy";
            (bool ret, Quaternion quat) = DcmFromAxis(x_dir, yCross, z_dir, order);
            if (ret)
            {
                mapping[RightShoulder].rotation = quat * Quaternion.Inverse(OriRot[RightShoulder]);
                mapping[RightShoulder].Rotate(-90, 0, 0);
            }
        }
        // RElbow
        {
            Vector3 x_dir = -initDir[RightHand];
            Vector3 y_dir = initDir[RightElbow];
            Vector3 yd2 = mapping[RightShoulder].up;
            Vector3 yc = Vector3.Cross(yd2, y_dir).normalized;
            Vector3 z_dir = Vector3.zero;
            string order = "xzy";
            (bool ret, Quaternion quat) = DcmFromAxis(x_dir, yd2, z_dir, order);
            if (ret)
            {
                mapping[RightElbow].rotation = quat * Quaternion.Inverse(OriRot[RightElbow]);
                mapping[RightElbow].Rotate(90, 0, 0);
            }
        }
        // LShoulder
        {
            Vector3 x_dir = initDir[LeftElbow];
            Vector3 y_dir = -initDir[Neck];
            Vector3 yd2 = initDir[LeftShoulder];
            Vector3 yc = Vector3.Cross(yd2, y_dir);
            Vector3 z_dir = Vector3.zero;
            string order = "xzy";
            (bool ret, Quaternion quat) = DcmFromAxis(x_dir, yc, z_dir, order);
            if (ret)
            {
                mapping[LeftShoulder].rotation = quat * Quaternion.Inverse(OriRot[LeftShoulder]);
                mapping[LeftShoulder].Rotate(-90, 0, 0);
            }
        }
        // LElbow
        {
            Vector3 x_dir = initDir[LeftHand];
            Vector3 y_dir = initDir[LeftElbow];
            Vector3 yd2 = mapping[LeftShoulder].up;
            Vector3 z_dir = Vector3.zero;
            string order = "xzy";
            (bool ret, Quaternion quat) = DcmFromAxis(x_dir, yd2, z_dir, order);
            if (ret)
            {
                mapping[LeftElbow].rotation = quat * Quaternion.Inverse(OriRot[LeftElbow]);
                mapping[LeftElbow].Rotate(90, 0, 0);
            }
        }

    }

    public void SetFrameFromSkeleton(Dictionary<string, SkeletonJoint> instanceJoints)
    {
        if (initSuccess)
        {
            mapping[Hip].position = instanceJoints[Hip].transform.position;
            foreach (KeyValuePair<string, Transform> item in mapping)
            {
                if (!item.Key.Contains("EndSite"))
                {
                    Quaternion qt = instanceJoints[item.Key].transform.rotation;
                    Quaternion newRotation = qt * OriRot[item.Key];
                    mapping[item.Key].rotation = newRotation;
                }
            }
        }
    }
    string LeftShoulder;
    string RightShoulder;
    string LeftElbow;
    string RightElbow;
    string LeftHand;
    string RightHand;
    string Neck;
    string Hip;

    public void TransformMap(JointInfo jointInfo)
    {
        if (!jointInfo.name.Contains("EndSite"))
        {
            if (target.map18.TryGetValue(jointInfo.name, out Transform jointTransform))
            {
                if (jointTransform == target.animator.GetBoneTransform(HumanBodyBones.Hips))
                {
                    Hip = jointInfo.name;
                }
                else if(jointTransform == target.animator.GetBoneTransform(HumanBodyBones.LeftUpperArm))
                {
                    LeftShoulder = jointInfo.name;
                }
                else if (jointTransform == target.animator.GetBoneTransform(HumanBodyBones.RightUpperArm))
                {
                    RightShoulder = jointInfo.name;
                }
                else if (jointTransform == target.animator.GetBoneTransform(HumanBodyBones.RightLowerArm))
                {
                    RightElbow = jointInfo.name;
                }
                else if (jointTransform == target.animator.GetBoneTransform(HumanBodyBones.LeftLowerArm))
                {
                    LeftElbow = jointInfo.name;
                }
                else if (jointTransform == target.animator.GetBoneTransform(HumanBodyBones.RightHand))
                {
                    RightHand = jointInfo.name;
                }
                else if (jointTransform == target.animator.GetBoneTransform(HumanBodyBones.LeftHand))
                {
                    LeftHand = jointInfo.name;
                }
                else if (jointTransform == target.animator.GetBoneTransform(HumanBodyBones.Neck))
                {
                    Neck = jointInfo.name;
                }
                mapping.Add(jointInfo.name, jointTransform);
                initDir.Add(jointInfo.name, jointInfo.offset);
            }
            else
            {

            }
            for (int i = 0; i < jointInfo.ChildJointsInfo.Count; i++)
            {
                TransformMap(jointInfo.ChildJointsInfo[i]);
            }
        }
        
    }
}
