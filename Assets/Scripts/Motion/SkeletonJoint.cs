﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonJoint : MonoBehaviour
{
    public Transform joint;
    public void SetSize(float size)
    {
        joint.localScale = Vector3.one * size;
    }
}
