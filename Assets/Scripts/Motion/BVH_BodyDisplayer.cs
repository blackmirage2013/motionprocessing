﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BVH_Motion;

public class BVH_BodyDisplayer : MonoBehaviour
{
    public JointInfo rootJointInfo;
    public Dictionary<string, SkeletonJoint> instanceJoints;
    public Dictionary<string, BVH_Motion.Joint> lastJoints = new Dictionary<string, BVH_Motion.Joint>();
    public Dictionary<string, BVH_Motion.JointInfo> lastJointInfos = new Dictionary<string, JointInfo>();
    public Dictionary<BVH_Motion.Joint, string> lastJointNames = new Dictionary<BVH_Motion.Joint, string>();
    public Dictionary<string, GameObject> instanceSkeletons;
    public BVH_CharacterDisplayer characterDisplayer;
    //public Dictionary<string, JointInfo> jointsInfo;
    public Object jointObject;
    public Object skeleton;
    public void Init(JointInfo rootInfo)
    {
        // 重置時移除原本骨架
        if (rootJointInfo != null && instanceJoints != null && instanceJoints[rootJointInfo.name] != null)
            DestroyImmediate(instanceJoints[rootJointInfo.name].gameObject);

        rootJointInfo = rootInfo;
        instanceJoints = new Dictionary<string, SkeletonJoint>();
        instanceJoints.Add(rootJointInfo.name, (Instantiate(jointObject, transform) as GameObject).GetComponent<SkeletonJoint>());
        instanceSkeletons = new Dictionary<string, GameObject>();
        //jointsInfo = new Dictionary<string, JointInfo>();
        //jointsInfo.Add(rootJointInfo.name, rootJointInfo);
        JointInstance(rootJointInfo);
    }

    public void JointInstance(JointInfo jointInfo)
    {
        for (int i = 0; i < jointInfo.ChildJointsInfo.Count; i++)
        {
            //jointsInfo.Add(jointInfo.ChildJointsInfo[i].name, jointInfo.ChildJointsInfo[i]);
            JointInfo childJointInfo = jointInfo.ChildJointsInfo[i];
            GameObject newJointInstance = Instantiate(jointObject, instanceJoints[childJointInfo.parentJointInfo.name].transform) as GameObject;
            newJointInstance.name = childJointInfo.name + "_Joint";
            instanceJoints.Add(childJointInfo.name, newJointInstance.GetComponent<SkeletonJoint>());

            GameObject newSkeleton = Instantiate(skeleton, instanceJoints[childJointInfo.parentJointInfo.name].transform) as GameObject;
            instanceSkeletons.Add(childJointInfo.name, newSkeleton); // 以Child的名字命名，才不會有重複問題

            JointInstance(childJointInfo);
        }
    }

    public void SetFrame(BVH_Motion.Joint joint, JointInfo jointInfo)
    {
        Transform jointInstance = instanceJoints[jointInfo.name].transform;
        jointInstance.localPosition = jointInfo.offset;
        jointInstance.localRotation = joint.rotation * joint.rotationDisplacement;
        lastJoints[jointInfo.name] = joint;
        lastJointInfos[jointInfo.name] = jointInfo;
        lastJointNames[joint] = jointInfo.name;

        for (int i = 0; i < jointInfo.ChildJointsInfo.Count; i++)
        {
            SetFrame(joint.ChildJoints[i], jointInfo.ChildJointsInfo[i]);
        }
    }
    public void SetOriginalFrame(BVH_Motion.Joint joint, JointInfo jointInfo)
    {
        Transform jointInstance = instanceJoints[jointInfo.name].transform;
        jointInstance.localPosition = jointInfo.offset;
        jointInstance.localRotation = joint.rotation;
        lastJoints[jointInfo.name] = joint;
        lastJointInfos[jointInfo.name] = jointInfo;
        lastJointNames[joint] = jointInfo.name;

        for (int i = 0; i < jointInfo.ChildJointsInfo.Count; i++)
        {
            SetOriginalFrame(joint.ChildJoints[i], jointInfo.ChildJointsInfo[i]);
        }
    }

    // 這裡應該可以直接拿世界旋轉?
    public void SetFrameSkeleton()
    {
        foreach (KeyValuePair<string, GameObject> item in instanceSkeletons)
        {
            Transform skeletonInstance = item.Value.transform;
            skeletonInstance.LookAt(instanceJoints[item.Key].transform.position, instanceJoints[item.Key].transform.up);
            skeletonInstance.Rotate(new Vector3(90, 0, 0));
            //skeletonInstance.rotation = instanceJoints[item.Key].transform.rotation;
            float size = Vector3.Distance(instanceJoints[item.Key].transform.position, skeletonInstance.position);  // 目標節點 / 父節點
            skeletonInstance.localScale = Vector3.one * size;
            float expDistance = Mathf.Log(size);
            float jointSize = Mathf.Pow(2, expDistance) * 0.075f;
            instanceJoints[item.Key].joint.localScale = Vector3.one * jointSize * 2f;

        }
    }

    public void SetFrame(RootJoint rootJoint)
    {
        SetFrame(rootJoint, rootJointInfo);
        Transform jointInstance = instanceJoints[rootJointInfo.name].transform;
        jointInstance.localPosition = rootJointInfo.offset + rootJoint.position + rootJoint.positionDisplacement;
        jointInstance.localRotation = rootJoint.rotation * rootJoint.rotationDisplacement;
        lastJoints[rootJointInfo.name] = rootJoint;
        lastJointInfos[rootJointInfo.name] = rootJointInfo;
        lastJointNames[rootJoint] = rootJointInfo.name;

        SetFrameSkeleton();
        characterDisplayer?.SetFrameFromSkeleton(instanceJoints);
        //
        // skeletonInstance.localPosition = jointInfo.ChildJointsInfo[i].offset; // Always from origin point
        // skeletonInstance.localRotation = joint.ChildJoints[i].rotation;
        //skeletonInstance.Rotate(new Vector3(0, 90, 0));

    }
    public void SetOriginalFrame(RootJoint rootJoint)
    {
        SetOriginalFrame(rootJoint, rootJointInfo);
        Transform jointInstance = instanceJoints[rootJointInfo.name].transform;
        jointInstance.localPosition = rootJointInfo.offset + rootJoint.position;
        jointInstance.localRotation = rootJoint.rotation;
        lastJoints[rootJointInfo.name] = rootJoint;
        lastJointInfos[rootJointInfo.name] = rootJointInfo;
        lastJointNames[rootJoint] = rootJointInfo.name;

        SetFrameSkeleton();
        characterDisplayer?.SetFrameFromSkeleton(instanceJoints);
        //
        // skeletonInstance.localPosition = jointInfo.ChildJointsInfo[i].offset; // Always from origin point
        // skeletonInstance.localRotation = joint.ChildJoints[i].rotation;
        //skeletonInstance.Rotate(new Vector3(0, 90, 0));

    }
    public Vector3[] GetBoundingBox()
    {
        Vector3[] minmaxPos = new Vector3[2];
        if (instanceJoints != null)
        {
            bool isFirst = true;
            foreach (SkeletonJoint joint in instanceJoints.Values)
            {
                if (isFirst)
                {
                    minmaxPos[0] = minmaxPos[1] = joint.transform.position;
                    isFirst = false;
                    continue;
                }

                Vector3 p = joint.transform.position;
                for (int i = 0; i < 3; ++i)
                {
                    if (p[i] < minmaxPos[0][i])
                        minmaxPos[0][i] = p[i];
                    if (p[i] > minmaxPos[1][i])
                        minmaxPos[1][i] = p[i];
                }
            }
        }
        return minmaxPos;
    }
}
