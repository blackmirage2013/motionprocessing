﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIHint : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    bool hint = false;
    public GameObject hint_Window;

    public void OnPointerEnter(PointerEventData eventData)
    {
        hint = true;
        hint_Window.SetActive(hint);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        hint = false;
        hint_Window.SetActive(hint);
    }
}
