﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MotionInterpolation : Tool
{
    public Image iconDisplay;
    public Text hintText;
    public Sprite interpolatioIcon_Enable;
    public Sprite interpolatioIcon_Disable;
    private bool interpolation = false;

    public override void Excute()// 執行功能
    {
        interpolation = !interpolation;
        if (interpolation)
        {
            iconDisplay.sprite = interpolatioIcon_Disable;
            hintText.text = "關閉動作插幀";
        }
        else
        {
            iconDisplay.sprite = interpolatioIcon_Enable;
            hintText.text = "開啟動作插幀";
        }
        for (int i=0;i < BVH_MotionViewer.allViewers.Count;i++)
        {
            BVH_MotionViewer.allViewers[i].interpolation = interpolation;
        }
    }
    public override void Cancel()// 關閉視窗
    {

    }
    public override void Confirm()// 打開視窗
    {
        Excute();
    }

}
