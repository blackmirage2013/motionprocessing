﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tool : MonoBehaviour
{
    public virtual void Cancel() // 關閉視窗
    {

    }
    public virtual void Excute() // 執行功能
    {

    }
    public virtual void Confirm() // 打開視窗
    {

    }
}
