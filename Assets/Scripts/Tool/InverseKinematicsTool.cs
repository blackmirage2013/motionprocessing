﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InverseKinematicsTool : Tool
{

    public override void Excute()// 執行功能
    {
        for (int i = 0; i < BVH_MotionViewer.allViewers.Count; i++)
        {
            BVH_MotionViewer.allViewers[i].solveIK();
        }
    }
    public override void Cancel()// 關閉視窗
    {

    }
    public override void Confirm()// 打開視窗
    {
        Excute();
    }

}

