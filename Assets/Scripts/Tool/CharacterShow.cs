﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterShow : Tool
{
    bool show = false;

    public override void Cancel() // 關閉視窗
    {

    }
    public override void Excute() // 執行功能
    {
        show = !show;
        for (int i = 0; i < BVH_MotionViewer.allViewers.Count; i++)
        {
            BVH_MotionViewer.allViewers[i].characterDisplayer.gameObject.SetActive(show);
            BVH_MotionViewer.allViewers[i].bodyDisplayer.gameObject.SetActive(!show);
        }
    }
    public override void Confirm() // 打開視窗
    {
        Excute();
    }
}
