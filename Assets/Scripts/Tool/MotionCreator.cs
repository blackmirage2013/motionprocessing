﻿using BVH_Motion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MotionCreator : Tool
{
    public Object motionObject;


    void SelectBaseMotionFile(string file)
    {
        BVH_Motion.Motion tmp = BVH_Motion.Motion.Load(file);
        if (tmp == null)
        {
            DialogManager.instance.NewDialog("錯誤提示", "BVH parser error");
        }
        else
        {
            GameObject newMotion = Instantiate(motionObject) as GameObject;
            newMotion.GetComponent<BVH_MotionViewer>().SetMotion(tmp);
            DialogManager.instance.NewDialog("載入提示", "載入成功");
        }
    }

    public void CreateBaseMotionSelector()
    {
        // 初始儲存位置
        string initPath = Folder.Motions_Folder;
        // 完成選擇
        List<UnityAction<string>> cfEvent = new List<UnityAction<string>>();
        cfEvent.Add(SelectBaseMotionFile);
        // 取消選擇
        List<UnityAction> cnEvent = new List<UnityAction>();
        // 選擇提示
        string[] requiredments = new string[2] { "未選擇原始動作(0/1)", "已選擇原始動作(1/1)" };
        // 要求檔案選擇器
        FileSelectManager.instance.NewFileSelector(FILE.BVH_MOTION, initPath, 1, 1, cfEvent, null, null, requiredments);
    }

    public override void Cancel() // 關閉視窗
    {

    }
    public override void Excute() // 執行功能
    {
        CreateBaseMotionSelector();
    }
    public override void Confirm() // 打開視窗
    {
        Excute();
    }
}
