﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearMotion : Tool
{
    public override void Cancel() // 關閉視窗
    {

    }
    public override void Excute() // 執行功能
    {
        for (int i = 0; i < BVH_MotionViewer.allViewers.Count; i++)
        {
            DestroyImmediate(BVH_MotionViewer.allViewers[i].gameObject);
        }
        BVH_MotionViewer.allViewers.Clear();
    }
    public override void Confirm() // 打開視窗
    {
        Excute();
    }
}
