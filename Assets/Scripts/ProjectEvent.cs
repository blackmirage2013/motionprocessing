﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using BVH_Motion;
using PathCreation;

[Serializable]
public class GetFilePaths : UnityEvent<string[]>
{

}

[Serializable]
public class FrameUpdate_Int : UnityEvent<int>
{

}

[Serializable]
public class GetCurrentlyPath : UnityEvent<string>
{

}

[Serializable]
public class FrameUpdate_Float : UnityEvent<float>
{

}

[System.Serializable]
public class FrameUpdate : UnityEvent<RootJoint>
{

}

[System.Serializable]
public class InfoFrameUpdate : UnityEvent<RootJoint, JointInfo>
{

}

[System.Serializable]
public class TransitionFrameUpdate : UnityEvent<RootJoint, RootJoint, JointInfo>
{

}

[System.Serializable]
public class PathUpdate : UnityEvent<BezierPath>
{

}


