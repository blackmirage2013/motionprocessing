﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FileItem : MonoBehaviour
{
    public Text info;
    private string filePath;
    public bool selected = false;
    public Toggle selectedDisplay;
    private FileSelector selector;
    public void SetInfo(string path, string name, FileSelector s)
    {
        selector = s;
        info.text = name;
        filePath = path;
    }

    public void OnClickFile()
    {
        if (selected)//已經選取則取消
        {
            selected = false;
            selectedDisplay.isOn = selected;
            if (!selector.Deselect(filePath))
                Debug.Log("bug");
        }
        else
        {
            if (selector.CanSelect(filePath))
            {
                selected = true;
                selectedDisplay.isOn = selected;
                selector.Select(filePath);
            }
        }
    }
}
