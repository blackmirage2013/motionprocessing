﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackItem : MonoBehaviour
{
    private FileBrowser browser;
    public void SetInfo(FileBrowser b)
    {
        browser = b;
    }

    public void Back()
    {
        browser.Back();
    }
}
