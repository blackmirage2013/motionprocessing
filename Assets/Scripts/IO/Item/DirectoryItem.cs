﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DirectoryItem : MonoBehaviour
{
    public Text info;
    private string directoryPath;
    private FileBrowser selector;
    public void SetInfo(string path, string name, FileBrowser s)
    {
        info.text = name;
        directoryPath = path;
        selector = s;
    }
    public void SetCurrentlyPath()
    {
        selector.currentlyPath = directoryPath;
        selector.UpdateList();
    }
}
