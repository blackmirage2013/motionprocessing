﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public enum FILE
{
    NONE = 0,
    BVH_MOTION = 1,
}


public class FileSelectManager : MonoBehaviour
{
    public static FileSelectManager instance;
    public FileBrowser fileBrowser;
    private void OnEnable()
    {
        instance = this;
    }

    public void NewFileBrowser( FILE viewFile, 
                                string initPath, 
                                List<UnityAction<string>> confirmEvents,
                                List<UnityAction> cancelEvents, 
                                string saveFile = null
                                )
    {
        FileBrowser browserInstance = Instantiate(fileBrowser, this.transform);
        GameObject instanceGamaobject = browserInstance.gameObject;
        browserInstance.Init(viewFile, FILE.NONE, initPath, confirmEvents, cancelEvents, saveFile);
    }

    public void NewFileSelector(FILE selectFile, // 選擇的檔案類型
                                string initPath, // 預設路徑
                                int minNum, //最小選擇數量
                                int maxNum, //最大選擇數量
                 
                                List<UnityAction<string>> confirmEvents = null, // 選擇確認事件(回傳單一結果)
                                List<UnityAction<string[]>> confirmMultiEvents = null, // 選擇確認事件(回傳複數結果)
                                List<UnityAction> cancelEvents = null, // 選擇取消事件
                                string[] requiredInfo = null // 選擇要求資訊
                                )
    {
        FileBrowser browserInstance = Instantiate(fileBrowser, this.transform);
        GameObject instanceGamaobject = browserInstance.gameObject;
        FileSelector selectorInstance = instanceGamaobject.AddComponent<FileSelector>();
        FileRequired requiredInstance = instanceGamaobject.AddComponent<FileRequired>();

        List<UnityAction<string[]>> completeSelection = new List<UnityAction<string[]>>();
        completeSelection.Add(requiredInstance.CompleteSelection);

        List<UnityAction> selectionUpdate = new List<UnityAction>();
        selectionUpdate.Add(requiredInstance.UpdateInfo);

        browserInstance.Init(FILE.NONE, selectFile, initPath, null, cancelEvents, null);
        selectorInstance.Init(minNum, maxNum, completeSelection, selectionUpdate, null);
        requiredInstance.Init(confirmEvents, confirmMultiEvents, requiredInfo);

        
    }



}
