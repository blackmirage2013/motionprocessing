﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(FileBrowser), typeof(FileSelector))]
public class FileRequired : MonoBehaviour
{
    private GetCurrentlyPath selectFile; 
    private GetFilePaths selectFiles;
    private List<string> displayInfo;
    private FileSelector selectorInstance;
    private FileBrowser browserInstance;

    private void OnEnable()
    {
        browserInstance = GetComponent<FileBrowser>();
        selectorInstance = GetComponent<FileSelector>();
        selectFile = new GetCurrentlyPath();
        selectFiles = new GetFilePaths();
    }

    public void Init(List<UnityAction<string>> confirmEvents, List<UnityAction<string[]>> confirmMultiEvents, string[] requiredInfo)
    {
        if (confirmEvents?.Count > 0)
        {
            foreach (UnityAction<string> action in confirmEvents)
            {
                selectFile.AddListener(action);
            }
        }
        if (confirmMultiEvents?.Count > 0)
        {
            foreach (UnityAction<string[]> action in confirmMultiEvents)
            {
                selectFiles.AddListener(action);
            }
        }
        if (requiredInfo != null)
            displayInfo = new List<string>(requiredInfo);
        else
            displayInfo = new List<string>();
        UpdateInfo();
    }

    public void CompleteSelection(string[] files)
    {
        if (files.Length == 1)
        {
            selectFile.Invoke(files[0]);
        }
        else
        {
            selectFiles.Invoke(files);
        }

        gameObject.GetComponent<FileSelector>().CloseSelector(true);
        Destroy(gameObject);
    }

    public void UpdateInfo()
    {
        if (displayInfo.Count > 0)
        {
            int index = selectorInstance.files.Count % displayInfo.Count;
            browserInstance.requiredInfo.text = displayInfo[index];
        }   
    }
}
