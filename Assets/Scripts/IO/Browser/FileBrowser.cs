﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class FileBrowser : MonoBehaviour
{
    public static FileBrowser instance;
    public string currentlyPath;
    public Text ShowPath;

    public Object fileItem; // 可選擇的預設物件
    public Object viewFileItem;
    public Object directoryItem;
    public Object backItem;
    public Transform Content;
    public Scrollbar scrollbar_FileBrowser;
    public FILE fileSelect;
    public FILE fileView;

    public Button confirmBtn;
    public Button cancelBtn;

    public InputField s_FileName;
    public Text requiredInfo;

    [Header("Enent")]
    public GetCurrentlyPath confirmEvent;
    public UnityEvent cancelEvent;

    private List<GameObject> fileList;
    private List<GameObject> directoryList;
    private bool save = false;
    public void Init(FILE viewFile, FILE selectFile, string initPath, List<UnityAction<string>> confirmEvents = null, List<UnityAction> cancelEvents = null, string saveFile = null)
    {
        currentlyPath = Application.dataPath; //設定起始位置
        fileList = new List<GameObject>();
        directoryList = new List<GameObject>();
        confirmEvent = new GetCurrentlyPath();
        cancelEvent = new UnityEvent();
        if (saveFile != null && saveFile != "")
        {
            requiredInfo.gameObject.SetActive(false);
            s_FileName.gameObject.SetActive(true);
            s_FileName.text = saveFile;
            save = true;
        }
        else
        {
            requiredInfo.gameObject.SetActive(true);
            s_FileName.gameObject.SetActive(false);
            requiredInfo.text = "選擇位置";
            save = false;
        }
        

        fileView = viewFile;
        fileSelect = selectFile;
        currentlyPath = initPath;

        confirmBtn.onClick.AddListener(OnClickConfirm);
        if (confirmEvents?.Count > 0)
        {
            for (int i = 0; i < confirmEvents.Count; i++)
            {
                confirmEvent.AddListener(confirmEvents[i]);
            }
        }
        cancelBtn.onClick.AddListener(OnClickCancel);
        if (cancelEvents?.Count > 0)
        {
            for (int i = 0; i < cancelEvents.Count; i++)
            {
                cancelEvent.AddListener(cancelEvents[i]);
            }
        }
        UpdateList();
    }

    public void OnClickConfirm()
    {
        if (save)
            confirmEvent.Invoke(System.IO.Path.Combine(currentlyPath, s_FileName.text));
        else
            confirmEvent.Invoke(currentlyPath);
        Destroy(gameObject);
    }

    public void OnClickCancel()
    {
        cancelEvent.Invoke();
        Destroy(gameObject);
    }

    public void ResetScrollbar()
    {
        scrollbar_FileBrowser.value = 1f;
    }

    public void Back()
    {
        if (currentlyPath == System.IO.Path.GetPathRoot(currentlyPath)) //不是root則新增Back
        {
            currentlyPath = "root";
            ShowPath.text = currentlyPath;

            ClearList();

            foreach (string filePath in System.IO.Directory.GetLogicalDrives())
            {
                GameObject newItem = Instantiate(directoryItem, Content) as GameObject;
                newItem.GetComponent<DirectoryItem>().SetInfo(filePath, filePath, this);
                directoryList.Add(newItem); 
            }

            ResetScrollbar();
        }
        else
        {
            currentlyPath = System.IO.Path.GetDirectoryName(currentlyPath);
            UpdateList();
        }
    }

    public void ClearList()//移除所有list內容
    {
        //移除原本所有資料夾內容
        for (int i = directoryList.Count; i > 0; i--)
        {
            Destroy(directoryList[i - 1]);
        }
        directoryList.Clear();
        //移除原本所有檔案內容
        for (int i = fileList.Count; i > 0; i--)
        {
            Destroy(fileList[i - 1]);
        }
        fileList.Clear();
        foreach (Transform child in Content)
        {
            Destroy(child.gameObject);
        }
    }

    public void UpdateList() //更新所有list內容
    {
        ShowPath.text = currentlyPath;

        ClearList();

        if (currentlyPath != @"/storage/emulated/0") //手機版不要超過這裡
        {
            GameObject newItem = Instantiate(backItem, Content) as GameObject;
            newItem.GetComponent<BackItem>().SetInfo(this);
            directoryList.Add(newItem);
        }
        //新增資料夾
        foreach (string directoryPath in System.IO.Directory.GetDirectories(currentlyPath))
        {
            try //嘗試讀取確認權限
            {
                System.Security.AccessControl.DirectorySecurity ds = System.IO.Directory.GetAccessControl(directoryPath);
            }
            catch (System.UnauthorizedAccessException)
            {
                continue; //不顯示資料夾
            }
            GameObject newItem = Instantiate(directoryItem, Content) as GameObject;
            newItem.GetComponent<DirectoryItem>().SetInfo(directoryPath, System.IO.Path.GetFileName(directoryPath), this);
            directoryList.Add(newItem);

        }

        //新增檔案
        InstantiateSelectable();
        InstantiateViewItem();
        //回到原位
        ResetScrollbar();
    }

    private void InstantiateViewItem()
    {
        if (fileSelect != fileView)
        {
            string[] extensions = new string[] { };
            if (fileView == FILE.BVH_MOTION)
                extensions = new string[] { "bvh" };
            foreach (string extension in extensions)
            {
                string fullExtension = "*." + extension;
                foreach (string filePath in System.IO.Directory.GetFiles(currentlyPath, fullExtension))
                {
                    GameObject newItem = Instantiate(viewFileItem, Content) as GameObject;
                    newItem.GetComponent<FileItem>().SetInfo(filePath, System.IO.Path.GetFileName(filePath), GetComponent<FileSelector>());
                    fileList.Add(newItem);
                }
            }
        }
    }



    private void InstantiateSelectable()
    {
        string[] extensions = new string[] { };
        if (fileSelect == FILE.BVH_MOTION)
            extensions = new string[] { "bvh" };
        foreach (string extension in extensions)
        {
            string fullExtension = "*." + extension;
            foreach (string filePath in System.IO.Directory.GetFiles(currentlyPath, fullExtension))
            {
                GameObject newItem = Instantiate(fileItem, Content) as GameObject;
                newItem.GetComponent<FileItem>().SetInfo(filePath, System.IO.Path.GetFileName(filePath), GetComponent<FileSelector>());
                fileList.Add(newItem);
            }
        }
    }

    private void OnEnable()
    {
        instance = this;
    }

   


}
