﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;



public class FileSelector : MonoBehaviour
{
    public static FileSelector instance;
    public List<string> files;
    public int minNum;
    public int maxNum;
    public GetFilePaths SelectionCompleted;    //當完成選擇後呼叫所有註冊function
    public UnityEvent SelectionUpdate;       //當選擇變動時呼叫所有註冊function
    public UnityEvent SelectionReset;       //當取消瀏覽時時呼叫所有註冊function


    public void Init(int minNum, int maxNum, List<UnityAction<string[]>> selectionCompleted, List<UnityAction> selectionUpdate, List<UnityAction> selectionReset)
    {
        this.minNum = minNum;
        this.maxNum = maxNum;
        files = new List<string>();
        SelectionCompleted = new GetFilePaths();
        FileBrowser browserInstance = GetComponent<FileBrowser>();
        browserInstance.confirmBtn.onClick.AddListener(CompleteSelection);
        browserInstance.cancelBtn.onClick.AddListener(() => { CloseSelector(true); });
        if (selectionCompleted?.Count > 0)
        {
            for(int i=0;i<selectionCompleted.Count;i++)
            {
                SelectionCompleted.AddListener(selectionCompleted[i]);
            }
        }
        SelectionUpdate = new UnityEvent();
        if (selectionUpdate?.Count > 0)
        {
            for (int i = 0; i < selectionUpdate.Count; i++)
            {
                SelectionUpdate.AddListener(selectionUpdate[i]);
            }
        }
        SelectionReset = new UnityEvent();
        if (selectionReset?.Count > 0)
        {
            for (int i = 0; i < selectionReset.Count; i++)
            {
                SelectionReset.AddListener(selectionReset[i]);
            }
        }
    }


    public virtual void OnEnable()
    {
        instance = this;
        //SelectionUpdate.Invoke();
    }

    public virtual bool CanSelect(string filePath)
    {
        if (maxNum == -1) // 沒有上限
            return true;
        else
            return files.Count < maxNum;
    }

    public virtual bool Deselect(string filePath)
    {
        if (files.Remove(filePath))
        {
            SelectionUpdate.Invoke();
            return true;
        }
        else
        {
            return false;
        }
    }

    public virtual void Select(string filePath)
    {
        files.Add(filePath);
        SelectionUpdate.Invoke();
    }

    public virtual void CompleteSelection()
    {
        if (maxNum == -1) // 沒有上限
        {
            if (files.Count >= minNum)
            {
                SelectionCompleted.Invoke(files.ToArray()); //告訴所有註冊腳本已完成選擇
                FileRequired test;
                if (TryGetComponent<FileRequired>(out test) == false)
                {
                    Destroy(test);
                }
            }
            else
            {
                DialogManager.instance.NewDialog("錯誤提示", "不滿足選擇條件", false);
            }
        }
        else
        {
            if (files.Count >= minNum && files.Count <= maxNum)
            {
                SelectionCompleted.Invoke(files.ToArray()); //告訴所有註冊腳本已完成選擇
                FileRequired test;
                if (TryGetComponent<FileRequired>(out test) == false)
                {
                    Destroy(test);
                }
            }
            else
            {
                DialogManager.instance.NewDialog("錯誤提示", "不滿足選擇條件", false);
            }
        }
    }



    public virtual void CloseSelector(bool Reset)
    {
        if (true)
        {
            SelectionReset.Invoke();
            files.Clear();
        }
        FileRequired test;
        if (TryGetComponent<FileRequired>(out test) == false)
        {
            Destroy(test);
        }
    }

    public void ClearFile()
    {
        files.Clear();
    }
}
