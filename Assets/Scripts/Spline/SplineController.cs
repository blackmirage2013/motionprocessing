﻿using PathCreation;
using PathCreation.Examples;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SplineController : PathSceneTool
{
    public List<AnchorPoint_Instance> anchorPoint = new List<AnchorPoint_Instance>();
    public BezierPath bezierPath;
    public AnchorPoint_Instance anchorPoint_Object;
    public PathUpdate pathUpdateEvent;


    protected override void PathUpdated()
    {
        if (pathCreator != null)
        {
            if (Application.isPlaying)
            {
                bezierPath = pathCreator.bezierPath;
                int intervel = 3;
                for (int i = 0; i * intervel < bezierPath.NumPoints; i++) // 每三個間隔新增一個控制點(顯示)
                {
                    if (i < anchorPoint.Count) // 舊物利用
                    {
                        anchorPoint[i].SetInfo(pathCreator, i * intervel);
                    }
                    else // 新增
                    {
                        AnchorPoint_Instance newPoint = Instantiate(anchorPoint_Object, transform);
                        newPoint.SetInfo(pathCreator, i * intervel);
                        anchorPoint.Add(newPoint);
                    }
                }
                for (int i = bezierPath.NumPoints; i < anchorPoint.Count; i++) // 多餘的控制點刪除
                {
                    Destroy(anchorPoint[i].gameObject);
                }
                if (anchorPoint.Count > bezierPath.NumPoints)
                {
                    anchorPoint.RemoveRange(bezierPath.NumPoints, anchorPoint.Count - bezierPath.NumPoints);
                }
                if (anchorPoint.Count > 2)
                {
                    pathUpdateEvent.Invoke(bezierPath);
                }
            }
        }
    }
}
