﻿using System.Collections.Generic;
using PathCreation;
using PathCreation.Examples;
using PathCreation.Utility;
using UnityEngine;

public class SplineRender: PathSceneTool
{
    public LineRenderer lineRenderer;
    public SplineController splineController;
    private Vector3 center;

    protected override void PathUpdated()
    {
        if (pathCreator != null)
        {
            CreateSplineRender();
        }
    }

    private void CreateSplineRender()
    {
        // 這裡不能直接拿點畫
        int pointNum = pathCreator.path.isClosedLoop ? pathCreator.path.NumPoints + 1 : pathCreator.path.NumPoints;
        Vector3[] verts = new Vector3[pointNum];
        center = Vector3.zero;

        for (int i = 0; i < pathCreator.path.NumPoints; i++)
        {
            verts[i] = pathCreator.path.GetPoint(i);
            center += verts[i];
        }
        if (pathCreator.path.isClosedLoop)
        {
            verts[pointNum - 1] = pathCreator.path.GetPoint(0);
        }
        center /= pointNum;
        // updtate verts
        float t = Time.realtimeSinceStartup;
        lineRenderer.positionCount = pointNum;
        lineRenderer.SetPositions(verts);
    }
    private void Update()
    {
        float distance = Vector3.Distance(Camera.main.transform.position, center);
        float expDistance = Mathf.Log(distance);
        float size = Mathf.Pow(2, expDistance) * 0.075f;
        lineRenderer.endWidth = size;
        lineRenderer.startWidth = size;
    }
}
