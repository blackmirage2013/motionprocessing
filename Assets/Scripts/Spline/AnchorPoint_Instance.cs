﻿using PathCreation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class AnchorPoint_Instance : MonoBehaviour
{
    public Transform ballVisualize;
    private PathCreator creator;
    private int index;
    private Vector3 mOffset;
    private float mZCoord;

    public void SetInfo(PathCreator c, int id)
    {
        creator = c;
        index = id;
        transform.position = creator.bezierPath.GetPoint(index);
    }
    void OnMouseDown()
    {
        mZCoord = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;
    }

    void OnMouseDrag()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            Vector3 p = hit.point;
            p.y = transform.position.y;
            transform.position = p;
            creator.bezierPath.MovePoint(index, transform.position, true);
            creator.CustomPathModified();
        }
    }


    private void Update()
    {
        float distance = Vector3.Distance(Camera.main.transform.position, transform.position);
        float expDistance = Mathf.Log(distance);
        float size = Mathf.Pow(2 , expDistance) * 0.2f;
        ballVisualize.localScale = Vector3.one * size;
    }
}
