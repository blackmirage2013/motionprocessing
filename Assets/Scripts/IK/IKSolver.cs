﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BVH_MotionViewer))]
public class IKSolver : MonoBehaviour
{
    BVH_MotionViewer motionViewer;
    Dictionary<string, List<List<(int, BVH_Motion.Joint)>>> groundJoints;
    [SerializeField]
    int maxIteration = 10;
    [SerializeField]
    float distance = 1;

    private void Awake()
    {
        motionViewer = GetComponent<BVH_MotionViewer>();
    }

    /*
     * Shall be called after FollowPath()
     * Solve IK
     */
    public void solveIK()
    {
        int originalFrame = motionViewer.frame;
        foreach (string key in groundJoints.Keys)
        {
            for (int i = 0; i < groundJoints[key].Count; ++i)
            {
                // Find average position
                Vector3 positionSum = new Vector3();
                for (int j = 0; j < groundJoints[key][i].Count; ++j)
                {
                    motionViewer.SetFrame(groundJoints[key][i][j].Item1);
                    positionSum += motionViewer.bodyDisplayer.instanceJoints[key].transform.position;
                }
                Vector3 targetPosition = positionSum / groundJoints[key][i].Count;

                // per-frame solution
                for (int j = 0; j < groundJoints[key][i].Count; ++j)
                {
                    motionViewer.SetFrame(groundJoints[key][i][j].Item1);

                    float distanceTolerance = distance * 0.05f;
                    for (int k = 0; k < maxIteration; ++k)
                    {
                        Vector3 originalPosition = motionViewer.bodyDisplayer.instanceJoints[key].transform.position;

                        BVH_Motion.Joint activeJoint = groundJoints[key][i][j].Item2.parent;
                        while (activeJoint != null && activeJoint.ChildJoints.Length < 2)
                        {
                            Vector3 nowPosition = motionViewer.bodyDisplayer.instanceJoints[key].transform.position;

                            string activeName = motionViewer.bodyDisplayer.lastJointNames[activeJoint];
                            Vector3 activePosition = motionViewer.bodyDisplayer.instanceJoints[activeName].transform.position;

                            activeJoint.rotationDisplacement = Quaternion.FromToRotation(
                                motionViewer.bodyDisplayer.instanceJoints[activeName].transform.parent.worldToLocalMatrix * (nowPosition - activePosition),
                                motionViewer.bodyDisplayer.instanceJoints[activeName].transform.parent.worldToLocalMatrix * (targetPosition - activePosition)
                                ) * activeJoint.rotationDisplacement;

                            motionViewer.bodyDisplayer.SetFrame(activeJoint, motionViewer.bodyDisplayer.lastJointInfos[activeName]);
                            activeJoint = activeJoint.parent;
                        }

                        Vector3 newPosition = motionViewer.bodyDisplayer.instanceJoints[key].transform.position;
                        if ((newPosition - originalPosition).magnitude < distanceTolerance)
                        {
                            break;
                        }
                    }
                }
            }
        }

        //// Find all joints
        //List<BVH_Motion.Joint>[] frameJoints = new List<BVH_Motion.Joint>[motionViewer.m.frameData.Count];
        //for (int i = 0; i < motionViewer.m.frameData.Count; ++i)
        //{
        //    motionViewer.SetFrame(i);
        //    List<BVH_Motion.Joint> allJoints = motionViewer.m.frameData[i].GetAllChildJoints();
        //    allJoints.Insert(0, motionViewer.m.frameData[i]);
        //    frameJoints[i] = allJoints;
        //}
        //// Gaussian filter per joint
        //for (int i = 0; i < frameJoints[0].Count; ++i)
        //{
        //    List<Quaternion> newRotations = new List<Quaternion>();
        //    for (int j = 0; j < frameJoints.Length; ++j)
        //    {
        //        Vector3 rotationSum = new Vector3();
        //        float weightSum = 0;

        //        Vector3 originalRotation = frameJoints[j][i].rotationDisplacement.eulerAngles;

        //        int halfFilterSize = motionViewer.filterSize / 2;
        //        if (j + halfFilterSize >= motionViewer.m.frameData.Count)
        //            halfFilterSize = motionViewer.m.frameData.Count - j - 1;
        //        if (j - halfFilterSize < 0)
        //            halfFilterSize = j;

        //        for (int k = -halfFilterSize; k <= halfFilterSize; ++k)
        //        {
        //            Vector3 rotation = frameJoints[j + k][i].rotationDisplacement.eulerAngles;
        //            float weight = Mathf.Exp(-k ^ 2);
        //            for (int l = 0; l < 3; ++l)
        //            {
        //                while (rotation[l] - originalRotation[l] >= 180) rotation[l] -= 360;
        //                while (rotation[l] - originalRotation[l] <= -180) rotation[l] += 360;
        //            }
        //            rotationSum += rotation * weight;
        //            weightSum += weight;
        //        }

        //        Vector3 newRotation = rotationSum / weightSum;

        //        newRotations.Add(Quaternion.Euler(newRotation));
        //    }
        //    for (int j = 0; j < frameJoints.Length; ++j)
        //        frameJoints[j][i].rotationDisplacement = newRotations[j];
        //}

        motionViewer.SetFrame(originalFrame);
    }

    /*
     * Shall be called after Normalize()
     * Find footprint*/
    public void findNearGroundJoint()
    {
        int originalFrame = motionViewer.frame;

        Dictionary<string, List<(int, BVH_Motion.Joint)>> potentialGroundJoints = new Dictionary<string, List<(int, BVH_Motion.Joint)>>();
        for (int i = 0; i < motionViewer.m.frameData.Count; ++i)
        {
            motionViewer.bodyDisplayer.SetOriginalFrame(motionViewer.m.frameData[i]);
            motionViewer.m.frameData[i].positionDisplacement = new Vector3();
            foreach (string key in motionViewer.bodyDisplayer.instanceJoints.Keys)
            {
                motionViewer.bodyDisplayer.lastJoints[key].rotationDisplacement = Quaternion.identity;
                if (Mathf.Abs(motionViewer.bodyDisplayer.instanceJoints[key].transform.position.y) < distance)
                {

                    if (!potentialGroundJoints.ContainsKey(key))
                        potentialGroundJoints[key] = new List<(int, BVH_Motion.Joint)>();
                    potentialGroundJoints[key].Add((i, motionViewer.bodyDisplayer.lastJoints[key]));
                }
            }
        }

        // Find continued ground joint
        groundJoints = new Dictionary<string, List<List<(int, BVH_Motion.Joint)>>>();
        foreach (string key in potentialGroundJoints.Keys)
        {
            groundJoints[key] = new List<List<(int, BVH_Motion.Joint)>>();
            List<(int, BVH_Motion.Joint)> group = new List<(int, BVH_Motion.Joint)>();
            int last = -2;
            foreach ((int, BVH_Motion.Joint) tuple in potentialGroundJoints[key])
            {
                if (tuple.Item1 != last + 1)
                {
                    if (group.Count > 1)
                        groundJoints[key].Add(group);
                    group = new List<(int, BVH_Motion.Joint)>();
                }
                group.Add(tuple);
                last = tuple.Item1;
            }
            if (group.Count > 1)
                groundJoints[key].Add(group);
        }

        motionViewer.SetFrame(originalFrame);
    }

    // Start is called before the first frame update

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }
}
