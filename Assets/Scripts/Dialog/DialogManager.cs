﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DialogManager : MonoBehaviour
{
    public static DialogManager instance;

    public Transform contents;
    public Dialog sample;

    public class QueryData
    {
        public QueryData(string t, string c, List<UnityAction> acceptEvent, List<UnityAction> rejectEvent)
        {
            title = t;
            content = c;
            m_acceptEvent = acceptEvent;
            m_rejectEvent = rejectEvent;
        }
        public string title;
        public string content;
        public List<UnityAction> m_acceptEvent;
        public List<UnityAction> m_rejectEvent;
    }

    private List<QueryData> waitingDialog;

    private void Start()
    {
        instance = this;
        waitingDialog = new List<QueryData>();
    }
   
    public Dialog NewDialog(string title, string content, bool reject = false) // 是否有拒絕選項
    {
        Dialog dialog = Instantiate(sample, contents);
        dialog.DialogInfo(title, content, reject);
        return dialog;
    }

    public void NewDialogFromOtherThread(string title, string content, List<UnityAction> acceptEvent, List<UnityAction> rejectEvent) 
    {
        waitingDialog.Add(new QueryData(title, content, acceptEvent, rejectEvent));
    }

    public void Update()
    {
        if (waitingDialog.Count != 0)
        {
            for (int i = 0; i < waitingDialog.Count; i++)
            {
                Dialog dialog = Instantiate(sample, contents);
                bool reject = false;
                if (waitingDialog[i].m_rejectEvent.Count > 0)
                    reject = true;
                dialog.DialogInfo(waitingDialog[i].title, waitingDialog[i].content, reject);
                for (int j=0;j< waitingDialog[i].m_acceptEvent.Count;j++)
                {
                    dialog.AddAcceptEvent(waitingDialog[i].m_acceptEvent[i]);
                }
                for (int j = 0; j < waitingDialog[i].m_rejectEvent.Count; j++)
                {
                    dialog.AddRejectEvent(waitingDialog[i].m_rejectEvent[i]);
                }
            }
            waitingDialog.Clear();
        }

    }
}
