﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Dialog : MonoBehaviour
{
    public Text o_title;
    public Text o_content;
    public GameObject o_acceptBtn;
    public GameObject o_rejectBtn;
    public Text t_acceptBtnText;
    public Text t_rejectBtnText;
    private UnityEvent m_acceptEvent;  // 按下Accept觸發事件
    private UnityEvent m_rejectEvent;  // 按下Reject觸發事件

    public void SetAcceptBtnText(string t)
    {
        t_acceptBtnText.text = t;
    }
    public void SetRejectBtnText(string t)
    {
        t_rejectBtnText.text = t;
    }

    public void DialogInfo(string t, string c, bool reject)
    {
        gameObject.SetActive(true);
        o_title.text = t;
        o_content.text = c;
        m_acceptEvent = new UnityEvent();
        m_rejectEvent = new UnityEvent();
        if (reject) // 有拒絕選項
            o_rejectBtn.SetActive(true);
        else
            o_rejectBtn.SetActive(false);
    }
    public void AddAcceptEvent(UnityAction act)
    {
        m_acceptEvent.AddListener(act);
    }
    public void AddRejectEvent(UnityAction act)
    {
        m_rejectEvent.AddListener(act);
    }
    public void Accept()
    {
        m_acceptEvent.Invoke();
        Destroy(gameObject);
    }
    public void Reject()
    {
        m_rejectEvent.Invoke();
        Destroy(gameObject);
    }
}
