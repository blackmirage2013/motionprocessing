﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BVH_Motion;

public class TpTest : MonoBehaviour
{
    public Object motionObject;

    public void Test()
    {
        if (BVH_MotionViewer.allViewers.Count >=2)
        {
            BVH_Motion.Motion m1, m2;
            m1 = BVH_MotionViewer.allViewers[0].m;
            m2 = BVH_MotionViewer.allViewers[1].m;

            TimeWarping t = new TimeWarping();
            t.Init(m1, m2); // Ref Con
            t.Do(m1.frames, m2.frames);
            BVH_Motion.Motion m1_ref = t.alignedRefMotion;
            BVH_Motion.Motion m2_con = t.alignedConMotion;


            Debug.Log(t.alignedConMotion.frameData.Count);
            Debug.Log(t.alignedConMotion.frames);
            // Debug.Log(t.costList.Count);
            BVH_Motion.Motion mergeResult = new BVH_Motion.Motion(t.alignedConMotion);

            /*Tuple<int, int> mergeIndex = new Tuple<int, int>(0, 0);
            float minDiff = float.MaxValue;
            Debug.Log(minDiff);
            Debug.Log(mergeIndex);*/
            t.costList.Reverse();
            t.refIndex.Reverse();
            t.conIndex.Reverse();
            bool refContinue = false;
            float r = (float)(t.refIndex[t.refIndex.Count - 1] - t.refIndex[0]) / t.refIndex.Count;
            float c = (float)(t.conIndex[t.conIndex.Count - 1] - t.conIndex[0]) / t.conIndex.Count;

            if (Mathf.Abs(r - 1f) < Mathf.Abs(c - 1f))
            {
                refContinue = true;
            }
            else
            {
                refContinue = false;
            }

            int ref_S = t.refIndex[0];
            int con_S = t.conIndex[0];

            int ref_E = t.refIndex[t.refIndex.Count - 1];
            int con_E = t.conIndex[t.conIndex.Count - 1];

            for (int i = 0; i < m2.frameData.Count; i++)
            {
                if (i < con_S)
                {
                    mergeResult.frameData.Add(RootJoint.Clone(m2.frameData[i]));
                }
            }
            if (refContinue)
            {
                float rangeCon = con_E - con_S;
                for (int i = 0; i < m1_ref.frameData.Count; i++)
                {
                    float ratio = ((float)t.conIndex[i] - (float)con_S) / (float)rangeCon; // 先靠近Ref
                    mergeResult.frameData.Add(RootJoint.Blend(m2_con.frameData[i], m1_ref.frameData[i], ratio));
                }
            }
            else // Con Continue
            {
                float rangeRef = ref_E - ref_S;
                for (int i = 0; i < m1_ref.frameData.Count; i++)
                {
                    float ratio = ((float)t.refIndex[i] - (float)ref_S) / (float)rangeRef; // 先靠近Ref
                    mergeResult.frameData.Add(RootJoint.Blend(m2_con.frameData[i], m1_ref.frameData[i], ratio));
                }
            }
            for (int i = 0; i < m1.frameData.Count; i++)
            {
                if (i > ref_E)
                {
                    mergeResult.frameData.Add(RootJoint.Clone(m1.frameData[i]));
                }
            }

            mergeResult.rootJointInfo = JointInfo.Clone(m1.rootJointInfo);

            DestroyImmediate(BVH_MotionViewer.allViewers[0].gameObject);
            DestroyImmediate(BVH_MotionViewer.allViewers[1].gameObject);
            BVH_MotionViewer.allViewers.RemoveAt(0);
            BVH_MotionViewer.allViewers.RemoveAt(0);


            GameObject newMotion = Instantiate(motionObject) as GameObject;
            newMotion.GetComponent<BVH_MotionViewer>().SetMotion(mergeResult);
            // DialogManager.instance.NewDialog("載入提示", "載入成功");


        }
    }

}