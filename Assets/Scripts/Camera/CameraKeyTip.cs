﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraKeyTip : MonoBehaviour
{
    public static CameraKeyTip instance;
    private void Awake()
    {
        instance = this;
    }

    public void setText(CameraMoveType moveType)
    {
        string text = "";
        switch (moveType)
        {
            case CameraMoveType.Stay:
                text = "[W][A][S][D] : Move camera horizontally.\n" + 
                       "[Left Shift] : Move camera down.\n"+
                       "[Space]      : Move camera up.\n"+
                       "[Scroll]     : Change move speed.\n" +
                       "[Mouse right]: Rotate view.";
                break;
            case CameraMoveType.Trace:
                text = "[Tab]        : Switch target.\n"+
                       "[Scroll]     : Room in/out.\n"+
                       "[Mouse right]: Rotate view.";
                break;
            case CameraMoveType.ViewAll:
                text = "[Scroll]     : Room in/out.\n" +
                       "[Mouse right]: Rotate view.";
                break;
        }
        GetComponent<Text>().text = text;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
