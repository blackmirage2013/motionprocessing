﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CameraMoveType { Stay, Trace, ViewAll };
public class CameraMoveManager : MonoBehaviour
{
    static CameraMoveManager instance;

    CameraMoveType moveType = CameraMoveType.Trace;
    public int traceIdx = 0;

    public float mouseRotationSpeed = 0.1f;
    public float cameraMoveSpeed = 10f;
    public float showSizeRatio = 0.8f;


    bool mouseDown = false;
    Vector2 mouseOriPos;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        CameraKeyTip.instance.setText(moveType);
    }

    // Update is called once per frame
    void Update()
    {
        // Direction
        bool mouseNowDown = Input.GetMouseButton(1);
        Vector2 mousePos = Input.mousePosition;
        if (!mouseDown && mouseNowDown)
        {
            mouseOriPos = mousePos;
        }
        if (mouseDown)
        {
            Vector2 deltaVec = mousePos - mouseOriPos;
            deltaVec *= mouseRotationSpeed;
            Vector3 nowVec = transform.localEulerAngles;
            nowVec += new Vector3(-deltaVec.y, deltaVec.x, 0);
            if (nowVec.x > 90) nowVec.x -= 360;
            if (nowVec.x > 80)
                nowVec.x = 80;
            if (nowVec.x < -80)
                nowVec.x = -80;
            transform.localEulerAngles = nowVec;
            mouseOriPos = mousePos;
        }
        mouseDown = mouseNowDown;

        switch (moveType)
        {
            case CameraMoveType.Stay:
                {
                    cameraMoveSpeed *= Mathf.Pow(1.1f, Input.mouseScrollDelta.y);

                    //Position
                    Vector3 moveVec = new Vector3();
                    if (Input.GetKey(KeyCode.W))
                    {
                        Vector3 forward = transform.forward;
                        forward.y = 0;
                        moveVec += forward.normalized * cameraMoveSpeed;
                    }
                    if (Input.GetKey(KeyCode.S))
                    {
                        Vector3 forward = transform.forward;
                        forward.y = 0;
                        moveVec -= forward.normalized * cameraMoveSpeed;
                    }
                    if (Input.GetKey(KeyCode.A))
                    {
                        Vector3 forward = transform.right;
                        forward.y = 0;
                        moveVec -= forward.normalized * cameraMoveSpeed;
                    }
                    if (Input.GetKey(KeyCode.D))
                    {
                        Vector3 forward = transform.right;
                        forward.y = 0;
                        moveVec += forward.normalized * cameraMoveSpeed;
                    }
                    if (Input.GetKey(KeyCode.LeftShift))
                    {
                        moveVec += new Vector3(0, -cameraMoveSpeed, 0);
                    }
                    if (Input.GetKey(KeyCode.Space))
                    {
                        moveVec += new Vector3(0, cameraMoveSpeed, 0);
                    }
                    transform.position += moveVec * Time.deltaTime;
                }
                break;
            case CameraMoveType.Trace:
                {
                    if (Input.GetKeyDown(KeyCode.Tab))
                    {
                        traceIdx = (traceIdx + 1) % BVH_MotionViewer.allViewers.Count;
                    }
                    showSizeRatio *= Mathf.Pow(0.9f, Input.mouseScrollDelta.y);

                    if (traceIdx < BVH_MotionViewer.allViewers.Count)
                    {
                        Vector3[] boundingBox = BVH_MotionViewer.allViewers[traceIdx].bodyDisplayer.GetBoundingBox();
                        float size = (boundingBox[1] - boundingBox[0]).magnitude;
                        Vector3 newPos = (boundingBox[0] + boundingBox[1]) / 2 - showSizeRatio * transform.forward * size / Mathf.Tan(GetComponent<Camera>().fieldOfView / 2 * Mathf.Deg2Rad);
                        float r = Mathf.Pow(1e-2f, Time.deltaTime);
                        if (mouseNowDown)
                            r = Mathf.Pow(1e-12f, Time.deltaTime);
                        transform.position = transform.position * r + newPos * (1 - r);
                    }
        			else
                    {
                        traceIdx = 0;
                    }

                    //Transition Ver
                    if (traceIdx < BVH_Transition_MotionViewer.allViewers.Count)
                    {
                        Vector3[] boundingBox = BVH_Transition_MotionViewer.allViewers[traceIdx].bodyDisplayer.GetBoundingBox();
                        float size = (boundingBox[1] - boundingBox[0]).magnitude;
                        Vector3 newPos = (boundingBox[0] + boundingBox[1]) / 2 - showSizeRatio * transform.forward * size / Mathf.Tan(GetComponent<Camera>().fieldOfView / 2 * Mathf.Deg2Rad);
                        float r = Mathf.Pow(1e-2f, Time.deltaTime);
                        if (mouseNowDown)
                            r = Mathf.Pow(1e-12f, Time.deltaTime);
                        transform.position = transform.position * r + newPos * (1 - r);
                    }                }
                break;
            case CameraMoveType.ViewAll:
                {
                    showSizeRatio *= Mathf.Pow(0.9f, Input.mouseScrollDelta.y);

                    if (BVH_MotionViewer.allViewers.Count > 0)
                    {
                        bool isFirst = true;
                        Vector3[] boundingBox = new Vector3[3];
                        for (int i = 0; i < BVH_MotionViewer.allViewers.Count; ++i)
                        {
                            BVH_BodyDisplayer potentialTarget = BVH_MotionViewer.allViewers[i].bodyDisplayer;
                            if (potentialTarget!=null)
                            {
                                Vector3[] boundingBoxTemp = potentialTarget.GetBoundingBox();
                                if (isFirst)
                                {
                                    boundingBox = boundingBoxTemp;
                                    isFirst = false;
                                    continue;
                                }
                                for (int j = 0; j < 3; ++j)
                                {
                                    if (boundingBoxTemp[0][j] < boundingBox[0][j])
                                        boundingBox[0][j] = boundingBoxTemp[0][j];
                                    if (boundingBoxTemp[1][j] > boundingBox[1][j])
                                        boundingBox[1][j] = boundingBoxTemp[1][j];
                                }
                            }
                        }

                        float size = (boundingBox[1] - boundingBox[0]).magnitude;
                        Vector3 newPos = (boundingBox[0] + boundingBox[1]) / 2 - showSizeRatio * transform.forward * size / Mathf.Tan(GetComponent<Camera>().fieldOfView / 2 * Mathf.Deg2Rad);
                        float r = Mathf.Pow(1e-1f, Time.deltaTime);
                        if (mouseNowDown)
                            r = Mathf.Pow(1e-12f, Time.deltaTime);
                        transform.position = transform.position * r + newPos * (1 - r);
                    }
                    //Transition Ver
                    if (BVH_Transition_MotionViewer.allViewers.Count > 0)
                    {
                        Vector3[] boundingBox = BVH_Transition_MotionViewer.allViewers[0].GetComponent<BVH_BodyDisplayer>().GetBoundingBox();
                        for (int i = 1; i < BVH_Transition_MotionViewer.allViewers.Count; ++i)
                        {
                            Vector3[] boundingBoxTemp = BVH_Transition_MotionViewer.allViewers[i].GetComponent<BVH_BodyDisplayer>().GetBoundingBox();
                            for (int j = 0; j < 3; ++j)
                            {
                                if (boundingBoxTemp[0][j] < boundingBox[0][j])
                                    boundingBox[0][j] = boundingBoxTemp[0][j];
                                if (boundingBoxTemp[1][j] > boundingBox[1][j])
                                    boundingBox[1][j] = boundingBoxTemp[1][j];
                            }
                        }

                        float size = (boundingBox[1] - boundingBox[0]).magnitude;
                        Vector3 newPos = (boundingBox[0] + boundingBox[1]) / 2 - showSizeRatio * transform.forward * size / Mathf.Tan(GetComponent<Camera>().fieldOfView / 2 * Mathf.Deg2Rad);
                        float r = Mathf.Pow(1e-1f, Time.deltaTime);
                        if (mouseNowDown)
                            r = Mathf.Pow(1e-12f, Time.deltaTime);
                        transform.position = transform.position * r + newPos * (1 - r);
                    }
                }
                break;
        }
    }
    public void SwitchMove()
    {
        moveType = (CameraMoveType)(((int)moveType + 1) % Enum.GetNames(typeof(CameraMoveType)).Length);
        CameraKeyTip.instance.setText(moveType);
    }
}
