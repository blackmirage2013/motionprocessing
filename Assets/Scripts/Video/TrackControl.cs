﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class TrackControl : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Slider slider;
    public UnityEvent startSliding;
    public UnityEvent stopSliding;
    public float Value
    {
        get { return slider.value / slider.maxValue; }
        set { slider.value = value; }
    }
    public float Frame
    {
        get { return slider.value; }
    }
    public void OnPointerDown(PointerEventData ped)
    {
        startSliding.Invoke();
    }

    public void OnPointerUp(PointerEventData ped)
    {
        stopSliding.Invoke();
    }

}
