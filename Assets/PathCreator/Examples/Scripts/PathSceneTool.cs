﻿using UnityEngine;

namespace PathCreation.Examples
{
    public abstract class PathSceneTool : MonoBehaviour
    {
        public event System.Action onDestroyed;
        public PathCreator pathCreator;
        public bool autoUpdate = true;
        bool isSubscribed;

        protected abstract void PathUpdated();

        protected VertexPath path {
            get {
                return pathCreator.path;
            }
        }

        public void TriggerUpdate() {
            PathUpdated();
        }


        protected virtual void OnDestroy() {
            if (onDestroyed != null) {
                onDestroyed();
            }
        }




        private void Start()
        {
            if (!isSubscribed)
            {
                TryFindPathCreator();
                Subscribe();
            }

            if (this.autoUpdate)
            {
                if (this.pathCreator != null)
                {
                    this.TriggerUpdate();
                    TriggerUpdate();
                }

            }
        }

        protected virtual void OnPathModified()
        {
            if (this.autoUpdate)
            {
                TriggerUpdate();
            }
        }

        protected virtual void OnEnable()
        {
            this.onDestroyed += OnToolDestroyed;

            if (TryFindPathCreator())
            {
                Subscribe();
                TriggerUpdate();
            }
        }

        void OnToolDestroyed()
        {
            if (this != null)
            {
                this.pathCreator.pathUpdated -= OnPathModified;
            }
        }


        protected virtual void Subscribe()
        {
            if (this.pathCreator != null)
            {
                isSubscribed = true;
                this.pathCreator.pathUpdated -= OnPathModified;
                this.pathCreator.pathUpdated += OnPathModified;
            }
        }

        bool TryFindPathCreator()
        {
            // Try find a path creator in the scene, if one is not already assigned
            if (this.pathCreator == null)
            {
                if (this.GetComponent<PathCreator>() != null)
                {
                    this.pathCreator = this.GetComponent<PathCreator>();
                }
                else if (FindObjectOfType<PathCreator>())
                {
                    this.pathCreator = FindObjectOfType<PathCreator>();
                }
            }
            return this.pathCreator != null;
        }
    }
}
