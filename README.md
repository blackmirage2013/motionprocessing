version 0.1(2022.10.10):
	1.FileSelector/FileBrowser
	2.DialogManager
	3.BVH讀檔
	4.BVH動作播放(時間控制) # 應該用錐形之類的顯示角度改變
version 0.2(2022.10.11):
	1.Displays ellipsoids or other rigid shapes for bones. 
version 0.2(2022.10.12):
	1.Basic path editing (with orientations handled correctly)
version 0.3(2022.10.12):
	1.Key frame animation interpolation
version 0.4(2022.10.14):
	1.Position motions (under user control), concatenate, and blend them